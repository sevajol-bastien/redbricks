# RedBricks

RedBricks is a crowdsourcing and crowdfunding platform for video games.  
It is developed by [Ethical Games](https://ethical-games.org) and released under the [GNU AGPLv3 license](LICENSE.txt).

## Project status

RedBricks has been on beta since October 2018, and was released on October 17th, 2019.

## Description

Our main objective is a greater involvement of the players in each project: we 
provide players-developers communication tools, several ways to support game 
creators and even the possibility to submit your own ideas!

Thanks to our new "brick" funding system, we offer an alternative path to video
game creation: a more ethical and humane work method for developers, supported 
step by step by the players. 

## Requirements

In order to work, RedBricks requires:

* PHP 7.2.1 or later
* Apache
* MySQL
* Composer

## Installation

Follow these steps to install RedBricks:

* clone the repository
* create a database and import the [redbricks.sql](redbricks.sql) file
* update the `config.php` file with your database information (host, database name, username and password)
* (optional) if you're willing to work on the Stripe and/or Facebook integrations, update the `cconfig.php` file with your credentials (this step is optional for local development with placeholder keys provided in the `config.php` file)
* install dependencies by running `composer install`
* host your website by running `php -S localhost:8000 -t public` or configure Apache to run a virtual host on the `public` subfolder

## How to contribute?

More information on [this page](CONTRIBUTING.md).

## Bug report

Bugs must be reported on the RedBricks platform: 
https://redbricks.games/home/redbricks-107/bugs

## Links

Site: https://redbricks.games  
Discord: https://discord.gg/dzA4JuQ