-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Hôte : redbricks.mysql.db
-- Généré le :  lun. 28 oct. 2019 à 19:42
-- Version du serveur :  5.6.39-log
-- Version de PHP :  7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `redbricks`
--

-- --------------------------------------------------------

--
-- Structure de la table `attachments`
--

CREATE TABLE `attachments` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `bug_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `badge`
--

CREATE TABLE `badge` (
  `id` int(11) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `name_fr` varchar(255) NOT NULL,
  `name_es` varchar(255) NOT NULL,
  `description_en` text NOT NULL,
  `description_fr` text NOT NULL,
  `description_es` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `badge`
--

INSERT INTO `badge` (`id`, `name_en`, `name_fr`, `name_es`, `description_en`, `description_fr`, `description_es`) VALUES
(1, 'early bird registration', 'inscription anticipée', 'early bird registration', 'You were there in the beginning, thank you!', 'Vous étiez là dès le début, merci !', 'You were there in the beginning, thank you!'),
(2, 'alpha tester', 'alpha testeur', 'alpha tester', 'Your game is on RedBricks since the beginning.', 'Votre jeu est sur RedBricks depuis le début.', 'Your game is on RedBricks since the beginning.');

-- --------------------------------------------------------

--
-- Structure de la table `badge_users`
--

CREATE TABLE `badge_users` (
  `id` int(11) NOT NULL,
  `badge_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `bugs`
--

CREATE TABLE `bugs` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `severity_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `frequency_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name_en` varchar(250) NOT NULL,
  `name_fr` varchar(255) NOT NULL,
  `name_es` varchar(255) NOT NULL,
  `slug` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `category`
--

INSERT INTO `category` (`id`, `name_en`, `name_fr`, `name_es`, `slug`) VALUES
(1, 'Other', 'Autre', 'Other', '0-other'),
(2, 'Artificial Intelligence', 'Intelligence Artificielle', 'Artificial Intelligence', 'artificial-intelligence'),
(3, 'Game engine', 'Moteur de jeu', 'Game engine', 'game-engine'),
(4, 'Networking', 'Réseau', 'Networking', 'networking'),
(5, 'Visual', 'Visuel', 'Visual', 'visual'),
(6, 'Level design', 'Level design', 'Level design', 'level-design'),
(7, 'Performance', 'Performance', 'Performance', 'performance'),
(8, 'Audio', 'Audio', 'Audio', 'audio'),
(9, 'Script', 'Script', 'Script', 'script'),
(10, 'Compatibility', 'Comptatibilité', 'Compatibility', 'compatibility');

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `game_id` int(11) NOT NULL,
  `feature_id` int(11) NOT NULL,
  `bug_id` int(11) NOT NULL,
  `news_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `contributor`
--

CREATE TABLE `contributor` (
  `id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `favorites`
--

CREATE TABLE `favorites` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `features`
--

CREATE TABLE `features` (
  `id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `project_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `goal` bigint(20) DEFAULT NULL,
  `deadline` datetime DEFAULT NULL COMMENT 'Date limite pour le financement ',
  `estimatedDate` datetime DEFAULT NULL COMMENT ' 	date de livraison estimée ',
  `delivery_date` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `abandonDate` datetime DEFAULT NULL,
  `abandonReason` varchar(100) DEFAULT NULL COMMENT 'Motif de l''abandon ',
  `archived` tinyint(2) NOT NULL DEFAULT '0',
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `features_content`
--

CREATE TABLE `features_content` (
  `id` int(11) NOT NULL,
  `feature_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `content` text NOT NULL,
  `langcode` varchar(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `features_summary`
--

CREATE TABLE `features_summary` (
  `id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `langcode` varchar(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `features_tag`
--

CREATE TABLE `features_tag` (
  `id` int(11) NOT NULL,
  `feature_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `frequency`
--

CREATE TABLE `frequency` (
  `id` int(11) NOT NULL,
  `name_en` varchar(250) NOT NULL,
  `name_fr` varchar(255) NOT NULL,
  `name_es` varchar(255) NOT NULL,
  `slug` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `frequency`
--

INSERT INTO `frequency` (`id`, `name_en`, `name_fr`, `name_es`, `slug`) VALUES
(1, 'Rare', 'Rare', 'Rare', 'rare'),
(3, 'Occasional', 'Occasionnelle', 'Occasional', 'occasional'),
(5, 'Regular', 'Régulière', 'Regular', 'regular'),
(9, 'Inevitable', 'Inévitable', 'Inevitable', 'inevitable'),
(0, 'Unknown', 'Inconnue', 'Unknown', 'unknown');

-- --------------------------------------------------------

--
-- Structure de la table `games`
--

CREATE TABLE `games` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `licence` int(11) NOT NULL DEFAULT '1',
  `graphics_licence` int(11) NOT NULL DEFAULT '1',
  `music_licence` int(11) NOT NULL DEFAULT '1',
  `funct_stat` varchar(255) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `fee` int(11) NOT NULL DEFAULT '0',
  `status_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `games_content`
--

CREATE TABLE `games_content` (
  `id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `langcode` varchar(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `lang`
--

CREATE TABLE `lang` (
  `id` int(11) NOT NULL,
  `code` varchar(2) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `lang`
--

INSERT INTO `lang` (`id`, `code`, `name`) VALUES
(1, 'en', 'english'),
(2, 'fr', 'français'),
(3, 'es', 'español');

-- --------------------------------------------------------

--
-- Structure de la table `licences`
--

CREATE TABLE `licences` (
  `id` int(11) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `name_fr` varchar(255) NOT NULL,
  `name_es` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `licences`
--

INSERT INTO `licences` (`id`, `name_en`, `name_fr`, `name_es`, `slug`) VALUES
(1, 'Not defined', 'Non défini', 'No definida', 'not-defined'),
(2, 'Proprietary license', 'Licence propriétaire', 'Licensia propietario', 'proprietary-license'),
(3, 'GNU GPLv3', 'GNU GPLv3', 'GNU GPLv3', 'gnu-gplv3'),
(4, 'GNU AGPLv3', 'GNU AGPLv3', 'GNU AGPLv3', 'gnu-agplv3'),
(5, 'GNU LGPLv3', 'GNU LGPLv3', 'GNU LGPLv3', 'gnu-lgplv3'),
(6, 'Apache License', 'Licence Apache', 'Apache License', 'apache-license'),
(7, 'MIT License', 'Licence MIT', 'Licencia MIT', 'mit-license'),
(8, 'Creative Commons', 'Creative Commons', 'Creative Commons', 'creative-commons'),
(9, 'Mozilla Public License', 'Mozilla Public License', 'Mozilla Public License', 'mozilla-public-license'),
(10, 'Unlicense', 'Unlicense', 'Unlicense', 'unlicense'),
(11, 'Other', 'Autre', 'Otro', 'l-other');

-- --------------------------------------------------------

--
-- Structure de la table `links_game_platform`
--

CREATE TABLE `links_game_platform` (
  `id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `platform_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `id_thread` int(11) NOT NULL,
  `id_from` int(11) NOT NULL,
  `message` text NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `content` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `mp` tinyint(1) NOT NULL DEFAULT '1',
  `badge` tinyint(1) NOT NULL DEFAULT '1',
  `new_brick` tinyint(1) NOT NULL DEFAULT '1',
  `change_brick` tinyint(1) NOT NULL DEFAULT '1',
  `new_bug` tinyint(1) NOT NULL DEFAULT '1',
  `change_bug` tinyint(1) NOT NULL DEFAULT '1',
  `news` tinyint(1) NOT NULL DEFAULT '1',
  `tip` tinyint(1) NOT NULL DEFAULT '1',
  `deadline` tinyint(1) NOT NULL DEFAULT '1',
  `collect` tinyint(1) NOT NULL DEFAULT '1',
  `comment` tinyint(1) NOT NULL DEFAULT '1',
  `comment_contri` tinyint(1) NOT NULL DEFAULT '1',
  `change_contri` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `pending_purchases`
--

CREATE TABLE `pending_purchases` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `feature_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `card_id` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `fee` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `platforms`
--

CREATE TABLE `platforms` (
  `id` int(11) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `name_fr` varchar(255) NOT NULL,
  `name_es` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `fa` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `platforms`
--

INSERT INTO `platforms` (`id`, `name_en`, `name_fr`, `name_es`, `slug`, `fa`) VALUES
(1, 'GNU Linux', 'GNU Linux', 'GNU Linux', 'gnu-linux', 'fab fa-linux'),
(2, 'Windows', 'Windows', 'Windows', 'windows', 'fab fa-windows'),
(3, 'macOS', 'macOS', 'macOS', 'macos', 'fab fa-apple'),
(4, 'Android', 'Android', 'Android', 'android', 'fab fa-android'),
(5, 'iOS', 'iOS', 'iOS', 'ios', 'fab fa-apple'),
(6, 'Web', 'Internet', 'Web', 'web', 'fas fa-globe'),
(7, 'Console', 'Console', 'Console', 'console', 'fas fa-gamepad'),
(8, 'Other', 'Autre', 'Other', 'p-other', 'far fa-question-circle');

-- --------------------------------------------------------

--
-- Structure de la table `prefix_social_media`
--

CREATE TABLE `prefix_social_media` (
  `id` int(11) NOT NULL,
  `media` varchar(250) NOT NULL,
  `address` varchar(250) NOT NULL,
  `icon` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `prefix_social_media`
--

INSERT INTO `prefix_social_media` (`id`, `media`, `address`, `icon`) VALUES
(1, 'Facebook', 'https://www.facebook.com/', 'fab fa-facebook'),
(2, 'Twitter', 'https://twitter.com/', 'fab fa-twitter-square'),
(3, 'LinkedIn', 'https://www.linkedin.com/company/', 'fab fa-linkedin'),
(4, 'Reddit', 'https://www.reddit.com/r/', 'fab fa-reddit-square'),
(5, 'GitHub', 'https://github.com/', 'fab fa-github-square'),
(100, 'Other', 'https://', 'fas fa-link'),
(7, 'Youtube', 'https://www.youtube.com/channel/', 'fab fa-youtube'),
(8, 'Twitch', 'https://twitch.tv/', 'fab fa-twitch');

-- --------------------------------------------------------

--
-- Structure de la table `proposals`
--

CREATE TABLE `proposals` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `survey_id` int(11) NOT NULL,
  `proposal` varchar(100) NOT NULL,
  `image` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `purchases`
--

CREATE TABLE `purchases` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `feature_id` int(11) DEFAULT '0',
  `game_id` int(11) DEFAULT NULL,
  `price` bigint(20) NOT NULL,
  `country` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `stripe_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `rewards`
--

CREATE TABLE `rewards` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `amount` int(11) NOT NULL,
  `contributors_max` int(11) DEFAULT NULL,
  `contributors_nb` int(11) NOT NULL DEFAULT '0',
  `product_id` varchar(255) NOT NULL,
  `plan_id` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `severity`
--

CREATE TABLE `severity` (
  `id` int(11) NOT NULL,
  `name_en` varchar(250) NOT NULL,
  `name_fr` varchar(255) NOT NULL,
  `name_es` varchar(255) NOT NULL,
  `slug` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `severity`
--

INSERT INTO `severity` (`id`, `name_en`, `name_fr`, `name_es`, `slug`) VALUES
(1, 'Minor', 'Mineur', 'Minor', 'minor'),
(5, 'Medium', 'Moyen', 'Medium', 'medium'),
(7, 'Major', 'Majeur', 'Major', 'major'),
(9, 'Critical', 'Critique', 'Critical', 'critical'),
(0, 'Unknown', 'Inconnu', 'Unknown', 'unknown');

-- --------------------------------------------------------

--
-- Structure de la table `social_media`
--

CREATE TABLE `social_media` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `address` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `name_fr` varchar(255) NOT NULL,
  `name_es` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `status`
--

INSERT INTO `status` (`id`, `name_en`, `name_fr`, `name_es`, `slug`) VALUES
(1, 'Awaiting Moderation', 'En attente de modération', 'Awaiting Moderation', 'awaiting-moderation'),
(2, 'Submitting', 'Proposé', 'Submitting', 'submit'),
(3, 'Funding', 'En financement', 'Funding', 'funding'),
(4, 'In development', 'En développement', 'In development', 'in-development'),
(5, 'Completed', 'Terminé', 'Completed', 'completed'),
(6, 'Abandoned', 'Abandonné', 'Abandoned', 'abandoned');

-- --------------------------------------------------------

--
-- Structure de la table `status_bug`
--

CREATE TABLE `status_bug` (
  `id` int(11) NOT NULL,
  `name_en` varchar(250) NOT NULL,
  `name_fr` varchar(255) NOT NULL,
  `name_es` varchar(255) NOT NULL,
  `slug` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `status_bug`
--

INSERT INTO `status_bug` (`id`, `name_en`, `name_fr`, `name_es`, `slug`) VALUES
(1, 'New', 'Nouveau', 'New', 'new'),
(2, 'Todo', 'A faire', 'Todo', 'todo'),
(3, 'Request for information', 'En attente d\'information', 'Request for information', 'request-for-information'),
(4, 'In going', 'En cours', 'In going', 'in-going'),
(5, 'On review', 'En revu de code', 'On review', 'on-review'),
(6, 'Declined', 'Refusé', 'Declined', 'declined'),
(7, 'Done', 'Corrigé', 'Done', 'done');

-- --------------------------------------------------------

--
-- Structure de la table `status_game`
--

CREATE TABLE `status_game` (
  `id` int(11) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `name_fr` varchar(255) NOT NULL,
  `name_es` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `status_game`
--

INSERT INTO `status_game` (`id`, `name_en`, `name_fr`, `name_es`) VALUES
(1, 'Idea', 'Idée', 'Idea'),
(2, 'Prototype', 'Prototype', 'Prototype'),
(0, 'Pause', 'En pause', 'Pause'),
(4, 'Alpha', 'Alpha', 'Alpha'),
(5, 'Beta', 'Bêta', 'Beta'),
(6, 'Early Access', 'Accès anticipé', 'Early Access'),
(7, 'Release', 'Sorti', 'Release'),
(8, 'Done', 'Terminé', 'Done'),
(9, 'Abandoned', 'Abandonné', 'Abandoned'),
(3, 'Pre-Alpha', 'Pré-Alpha', 'Pre-Alpha');

-- --------------------------------------------------------

--
-- Structure de la table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `reward_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `subscription_id` varchar(255) NOT NULL,
  `plan_id` varchar(255) NOT NULL,
  `stripe_account` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `suggestion`
--

CREATE TABLE `suggestion` (
  `id` int(11) NOT NULL,
  `game` varchar(100) NOT NULL,
  `studio` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `support_articles`
--

CREATE TABLE `support_articles` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `name_fr` varchar(255) NOT NULL,
  `slug_fr` varchar(255) NOT NULL,
  `content_fr` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `support_sections`
--

CREATE TABLE `support_sections` (
  `id` int(11) NOT NULL,
  `name_fr` varchar(255) NOT NULL,
  `slug_fr` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `survey`
--

CREATE TABLE `survey` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `deadline` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `name_fr` varchar(255) NOT NULL,
  `name_es` varchar(255) NOT NULL,
  `game_id` int(11) NOT NULL,
  `color` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `threads`
--

CREATE TABLE `threads` (
  `id` int(11) NOT NULL,
  `id_users` text NOT NULL,
  `last_message` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `thread_unread`
--

CREATE TABLE `thread_unread` (
  `id` int(11) NOT NULL,
  `thread_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fb_id` varchar(50) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `displayname` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `profile` longtext,
  `location` varchar(255) DEFAULT NULL,
  `role` varchar(255) NOT NULL DEFAULT 'user',
  `stripe_user_id` varchar(21) DEFAULT NULL,
  `password_reset` varchar(255) DEFAULT NULL,
  `password_reset_at` datetime DEFAULT NULL,
  `activation_key` varchar(32) DEFAULT NULL,
  `active` tinyint(4) NOT NULL,
  `langcode` varchar(2) NOT NULL DEFAULT 'en',
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `users_stripe`
--

CREATE TABLE `users_stripe` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `customer_id` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `votes`
--

CREATE TABLE `votes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `feature_id` int(11) NOT NULL,
  `vote` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `votes_suggestion`
--

CREATE TABLE `votes_suggestion` (
  `id` int(11) NOT NULL,
  `suggestion_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `votes_survey`
--

CREATE TABLE `votes_survey` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `survey_id` int(11) NOT NULL,
  `proposal_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `attachments`
--
ALTER TABLE `attachments`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `badge`
--
ALTER TABLE `badge`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `badge_users`
--
ALTER TABLE `badge_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UQ_badgeId_userId` (`badge_id`,`user_id`);

--
-- Index pour la table `bugs`
--
ALTER TABLE `bugs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNI_PROJ_TITLE` (`project_id`,`title`(50));

--
-- Index pour la table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `contributor`
--
ALTER TABLE `contributor`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `features_content`
--
ALTER TABLE `features_content`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `features_summary`
--
ALTER TABLE `features_summary`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `features_tag`
--
ALTER TABLE `features_tag`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UC_featureId_tagId` (`feature_id`,`tag_id`);

--
-- Index pour la table `frequency`
--
ALTER TABLE `frequency`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `games_content`
--
ALTER TABLE `games_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gameId` (`game_id`);

--
-- Index pour la table `lang`
--
ALTER TABLE `lang`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `licences`
--
ALTER TABLE `licences`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Index pour la table `links_game_platform`
--
ALTER TABLE `links_game_platform`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `game_id` (`game_id`,`platform_id`);

--
-- Index pour la table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Index pour la table `pending_purchases`
--
ALTER TABLE `pending_purchases`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `platforms`
--
ALTER TABLE `platforms`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Index pour la table `prefix_social_media`
--
ALTER TABLE `prefix_social_media`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `proposals`
--
ALTER TABLE `proposals`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `feature_id` (`feature_id`) USING BTREE;

--
-- Index pour la table `rewards`
--
ALTER TABLE `rewards`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `severity`
--
ALTER TABLE `severity`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `social_media`
--
ALTER TABLE `social_media`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Index pour la table `status_bug`
--
ALTER TABLE `status_bug`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `status_game`
--
ALTER TABLE `status_game`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `suggestion`
--
ALTER TABLE `suggestion`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `support_articles`
--
ALTER TABLE `support_articles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `support_sections`
--
ALTER TABLE `support_sections`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `survey`
--
ALTER TABLE `survey`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `threads`
--
ALTER TABLE `threads`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `thread_unread`
--
ALTER TABLE `thread_unread`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNI_THREAD_USER` (`thread_id`,`user_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`,`username`);

--
-- Index pour la table `users_stripe`
--
ALTER TABLE `users_stripe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Index pour la table `votes`
--
ALTER TABLE `votes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `vote_unique` (`user_id`,`feature_id`);

--
-- Index pour la table `votes_suggestion`
--
ALTER TABLE `votes_suggestion`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `votes_survey`
--
ALTER TABLE `votes_survey`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `attachments`
--
ALTER TABLE `attachments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `badge`
--
ALTER TABLE `badge`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `badge_users`
--
ALTER TABLE `badge_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT pour la table `bugs`
--
ALTER TABLE `bugs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;
--
-- AUTO_INCREMENT pour la table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;
--
-- AUTO_INCREMENT pour la table `contributor`
--
ALTER TABLE `contributor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT pour la table `features`
--
ALTER TABLE `features`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=297;
--
-- AUTO_INCREMENT pour la table `features_content`
--
ALTER TABLE `features_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=297;
--
-- AUTO_INCREMENT pour la table `features_summary`
--
ALTER TABLE `features_summary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `features_tag`
--
ALTER TABLE `features_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `games`
--
ALTER TABLE `games`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;
--
-- AUTO_INCREMENT pour la table `games_content`
--
ALTER TABLE `games_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT pour la table `lang`
--
ALTER TABLE `lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `licences`
--
ALTER TABLE `licences`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `links_game_platform`
--
ALTER TABLE `links_game_platform`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=172;
--
-- AUTO_INCREMENT pour la table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT pour la table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;
--
-- AUTO_INCREMENT pour la table `pending_purchases`
--
ALTER TABLE `pending_purchases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT pour la table `platforms`
--
ALTER TABLE `platforms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `proposals`
--
ALTER TABLE `proposals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT pour la table `purchases`
--
ALTER TABLE `purchases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT pour la table `rewards`
--
ALTER TABLE `rewards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `social_media`
--
ALTER TABLE `social_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=397;
--
-- AUTO_INCREMENT pour la table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `status_bug`
--
ALTER TABLE `status_bug`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `suggestion`
--
ALTER TABLE `suggestion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `support_articles`
--
ALTER TABLE `support_articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT pour la table `support_sections`
--
ALTER TABLE `support_sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `survey`
--
ALTER TABLE `survey`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `threads`
--
ALTER TABLE `threads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `thread_unread`
--
ALTER TABLE `thread_unread`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=285;
--
-- AUTO_INCREMENT pour la table `users_stripe`
--
ALTER TABLE `users_stripe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT pour la table `votes`
--
ALTER TABLE `votes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;
--
-- AUTO_INCREMENT pour la table `votes_suggestion`
--
ALTER TABLE `votes_suggestion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `votes_survey`
--
ALTER TABLE `votes_survey`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `purchases`
--
ALTER TABLE `purchases`
  ADD CONSTRAINT `purchases_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `users_stripe`
--
ALTER TABLE `users_stripe`
  ADD CONSTRAINT `users_stripe_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
