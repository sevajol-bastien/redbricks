<?php

namespace App\About\Actions;

use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface;
use Framework\Response\RedirectResponse;

class AboutSecurityAction
{
    /**
     * @var RendererInterface
     */
    private $renderer;

    public function __construct(
        RendererInterface $renderer
    ) {
        $this->renderer = $renderer;
    }

    /**
     * @param ServerRequestInterface $request
     * @return RedirectResponse|string
     */
    public function __invoke(ServerRequestInterface $request)
    {
        return $this->renderer->render('@about/security');
    }
}
