<?php
namespace App\Account;

use Framework\Upload;

class AccountUpload extends Upload
{

    protected $path= "public/uploads/avatars";

    protected $formats = [
        "thumb" => [120, 120]
    ];
}
