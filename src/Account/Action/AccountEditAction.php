<?php

namespace App\Account\Action;

use App\Auth\UserTable;
use Framework\Auth;
use Framework\Renderer\RendererInterface;
use Framework\Response\RedirectResponse;
use Framework\Session\FlashService;
use Framework\Validator;
use Psr\Http\Message\ServerRequestInterface;
use App\Account\AccountUpload;

class AccountEditAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var Auth
     */
    private $auth;

    /**
     * @var FlashService
     */
    private $flashService;

    /**
     * @var UserTable
     */
    private $userTable;

    /**
     *
     * @var AccountUpload
     */
    protected $accountUpload;

    public function __construct(
            RendererInterface $renderer, Auth $auth, FlashService $flashService, UserTable $userTable, AccountUpload $accountUpload
    ) {

        $this->renderer = $renderer;
        $this->auth = $auth;
        $this->flashService = $flashService;
        $this->userTable = $userTable;
        $this->accountUpload = $accountUpload;
    }

    public function __invoke(ServerRequestInterface $request) {
        $user = $this->auth->getUser();
        //var_dump($request);die();
        $params = array_merge($request->getParsedBody(), $request->getUploadedFiles());
        //var_dump($params);die();
        if (isset($params['btn_account'])) {
            $validator = (new Validator($params))
                    ->confirm('password')
                    ->required('email');
            if (!empty($params['password'])) {
                $validator = $validator->length('password', '3');
            }
            if ($validator->isValid()) {
                $userParams = ['email' => $params['email']];
                if (!empty($params['password'])) {
                    $userParams['password'] = password_hash($params['password'], PASSWORD_DEFAULT);
                }
                $this->userTable->update($user->getId(), $userParams);
                $text = _("Your account has been updated");
                $this->flashService->success($text);
                return new RedirectResponse($request->getUri()->getPath());
            }
            $errors = $validator->getErrors();
            return $this->renderer->render('@account/account', compact('user', 'errors'));
        } elseif (isset($params['btn_profile'])) {
            $userParams = [
                'displayname' => $params['displayname'],
                'website' => $params['website'],
                'profile' => $params['profile'],
                'location' => $params['location']
            ];
            $avatar = $this->accountUpload->upload($params["avatar"], null);
            if ($avatar) {
                $userParams["avatar"] = $avatar;
            }
            $this->userTable->update($user->getId(), $userParams);
            $text = _("Your profile has been updated");
            $this->flashService->success($text);
            return new RedirectResponse($request->getUri()->getPath());
        }
    }

}
