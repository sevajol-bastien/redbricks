<?php

namespace App\Auth\Action;

use App\Auth\UserTable;
use Framework\Actions\RouterAwareAction;
use Framework\Database\NoRecordException;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Framework\Session\FlashService;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Expressive\Router\RouterInterface;
use App\Notification\Table\NotificationTable;

class AuthenticationAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var UserTable
     */
    private $userTable;

    /**
     *
     * @var NotificationTable
     */
    protected $notificationTable;

    /**
     * @var FlashService
     */
    private $flashService;

    /**
     * @var RouterInterface
     */
    private $router;

    use RouterAwareAction;

    public function __construct(
            RendererInterface $renderer,
            UserTable $userTable,
            FlashService $flashService,
            Router $router,
            NotificationTable $notificationTable) {
        $this->renderer = $renderer;
        $this->userTable = $userTable;
        $this->flashService = $flashService;
        $this->router = $router;
        $this->notificationTable = $notificationTable;
    }

    public function __invoke(ServerRequestInterface $request) {
        $params = $request->getQueryParams();
        try {
            $user = $this->userTable->findBy('username', $params['log']);
            if ($user->getActivationKey() == $params['key'] && !$user->isActive()) {
                $this->userTable->activateAccount($user->getId());
                $this->notificationTable->insert([
                    "user_id" => $user->getId()
                ]);
                $text = _("Your account has been activated, you can to log in.");
                $this->flashService->success($text);
                return $this->redirect('auth.login');
            }
            $this->flashService->error(_('The key is wrong or this account is already active'));
        } catch (NoRecordException $exception) {
            $this->flashService->error(_("This account doesn't exist"));
        }
        return $this->renderer->render('@auth/authentication');
    }

}
