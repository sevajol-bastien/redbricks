<?php

namespace App\Auth\Action;

use App\Auth\DatabaseAuth;
use Framework\Actions\RouterAwareAction;
use Framework\Renderer\RendererInterface;
use Framework\Response\RedirectResponse;
use Framework\Router;
use Framework\Session\FlashService;
use Framework\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Expressive\Router\RouterInterface;

class LoginAttemptAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var DatabaseAuth
     */
    private $auth;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var RouterInterface
     */
    private $router;

    use RouterAwareAction;

    public function __construct(
            RendererInterface $renderer,
            DatabaseAuth $auth,
            Router $router,
            SessionInterface $session
    ) {

        $this->renderer = $renderer;
        $this->auth = $auth;
        $this->router = $router;
        $this->session = $session;
    }

    public function __invoke(ServerRequestInterface $request) {
        //var_dump($this->session->get("routeReturn"));die();
        $params = $request->getParsedBody();
        //var_dump($params);die();
        $remember = isset($params['remember']);
        $user = $this->auth->login($params['username'], $params['password'], $remember);
        if ($user) {
            $role = $this->auth->getUser()->getRole();
            $path = $this->session->get('auth.redirect') ?: $this->router->generateUri($role);
            $this->session->delete('auth.redirect');
            if ($this->session->get("routeReturn") != null) {
                $path = $this->session->get("routeReturn");
                $this->session->delete('routeReturn');
                return $this->redirect($path);
            }
            //var_dump($this->session,$path);die();
            return new RedirectResponse($path);
        } else {
            (new FlashService($this->session))->error(_('Wrong username or password'));
            return $this->redirect('auth.login');
        }
    }

}
