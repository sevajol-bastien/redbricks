<?php

namespace App\Auth\Action;

use Framework\Renderer\RendererInterface;
use Framework\Api\FacebookConnect;
use Framework\Router;
use Framework\Actions\RouterAwareAction;
use Psr\Http\Message\ServerRequestInterface;
use App\Auth\UserTable;
use Framework\Database\NoRecordException;
use Framework\Session\SessionInterface;
use Framework\Response\RedirectResponse;
use Framework\Session\FlashService;

class LoginFacebookAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     *
     * @var UserTable
     */
    protected $userTable;

    /**
     *
     * @var FacebookConnect
     */
    protected $facebook;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     *
     * @var SessionInterface
     */
    protected $session;

    use RouterAwareAction;

    public function __construct(RendererInterface $renderer, FacebookConnect $facebook, Router $router, UserTable $userTable, SessionInterface $session) {
        $this->renderer = $renderer;
        $this->facebook = $facebook;
        $this->router = $router;
        $this->userTable = $userTable;
        $this->session = $session;
    }

    public function __invoke(ServerRequestInterface $request) {
        $userFB = $this->facebook->connect();
        if ($userFB) {
            $user = $this->findUser($userFB);
            $this->session->set('auth.user', $user->getId());
            $this->session->set('auth.role', $user->getRole());
            setcookie('auth', $user->getId() . '--' . sha1($user->getUsername() . $user->getPassword() . $_SERVER['REMOTE_ADDR']), time() + 3600 * 24 * 3, '/', 'redbricks.games', true, true);
            $path = $this->session->get('auth.redirect') ?: $this->router->generateUri("user");
            $this->session->delete('auth.redirect');
            if ($this->session->get("routeReturn") != null) {
                $path = $this->session->get("routeReturn");
                $this->session->delete('routeReturn');
                return $this->redirect($path);
            }
            return new RedirectResponse($path);
        } else {
            (new FlashService($this->session))->error(_('Error'));
            return $this->redirect('auth.login');
        }
    }

    private function findUser($userFB) {
        $fbId = $userFB->getId();
        try {
            $user = $this->userTable->findBy('fb_id', $fbId);
        } catch (NoRecordException $exception) {
            $name = $userFB->getName();
            $email = $userFB->getEmail();
            $this->userTable->insert([
                "fb_id" => $fbId,
                "username" => $name . rand(1, 300),
                "email" => $email,
                "password" => password_hash(uniqid(), PASSWORD_DEFAULT),
                "displayname" => $name,
                "role" => "user",
                "active" => 1,
                "created_at" => (new \DateTime())->format("Y-m-d H:i:s")
            ]);
            $user = $this->userTable->find($this->userTable->getPdo()->lastInsertId());
        }
        return $user;
    }

}
