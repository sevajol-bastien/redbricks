<?php

namespace App\Badge;

use Framework\Module;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Framework\Auth\LoggedInMiddleware;
use App\Badge\Actions\BadgeAction;
use App\Badge\Actions\AddBadgeAction;

class BadgeModule extends Module {

    public function __construct(Router $router, RendererInterface $renderer) {
        $renderer->addPath('badge', __DIR__ . "/views");
        $router->get('/badges', [LoggedInMiddleware::class, BadgeAction::class], 'badges');
    }

}
