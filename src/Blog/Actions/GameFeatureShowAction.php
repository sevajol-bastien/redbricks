<?php

namespace App\Blog\Actions;

use App\Blog\Table\VotesTable;
use App\Blog\Table\StatusTable;
use App\Blog\Table\FeatureTable;
use App\Shop\Table\PendingPurchaseTable;
use App\Shop\Table\PurchaseTable;
use App\Blog\Table\GameTable;
use App\Comments\Table\CommentsTable;
use App\Auth\UserTable;
use App\Blog\Table\FeatureTagTable;
use Framework\Actions\RouterAwareAction;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Framework\Auth;
use Psr\Http\Message\ServerRequestInterface as Request;

class GameFeatureShowAction {

    protected $routePrefix = "blog";
    //protected $routeReturn;  
    //protected $paramsPath;	

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var Router
     */
    private $router;

    /**
     *
     * @var CommentsTable
     */
    protected $commentsTable;

    /**
     *
     * @var UserTable
     */
    protected $userTable;

    /**
     *
     * @var PendingPurchaseTable
     */
    protected $pendingPurchaseTable;

    /**
     *
     * @var PurchaseTable
     */
    protected $purchaseTable;

    /**
     *
     * @var FeatureTagTable
     */
    protected $featureTagTable;

    /**
     *
     * @var Auth
     */
    protected $auth;

    use RouterAwareAction;

    public function __construct(
            RendererInterface $renderer, Router $router, GameTable $gameTable, FeatureTable $featureTable, StatusTable $statusTable, VotesTable $votesTable, PendingPurchaseTable $pendingPurchaseTable, PurchaseTable $purchaseTable, CommentsTable $commentsTable, UserTable $userTable, FeatureTagTable $featureTagTable, Auth $auth
    ) {
        $this->renderer = $renderer;
        $this->router = $router;
        $this->gameTable = $gameTable;
        $this->featureTable = $featureTable;
        $this->statusTable = $statusTable;
        $this->votesTable = $votesTable;
        $this->pendingPurchaseTable = $pendingPurchaseTable;
        $this->purchaseTable = $purchaseTable;
        $this->commentsTable = $commentsTable;
        $this->userTable = $userTable;
        $this->featureTagTable = $featureTagTable;
        $this->auth = $auth;
    }

    /**
     * Affiche un élément
     * @param Request $request
     * @return  ResponseInterface/string
     */
    public function __invoke(Request $request) {
        $slug = $request->getAttribute("slug");
        $id = $request->getAttribute("id");
        $feature = $this->featureTable->findOneShow($id);
        $game = $this->gameTable->findShow($feature->getProjectId());
        $status = $this->statusTable->findAll();
        $votes = $this->votesTable->findBy("feature_id", $feature->getId());
        $votesYes = $this->votesTable->findBy("feature_id", $feature->getId())->where("vote = 1")->count("*");
        $votesNo = $this->votesTable->findBy("feature_id", $feature->getId())->where("vote = 0")->count("*");
        $moneyRaised = $this->pendingPurchaseTable->getMoneyRaised("feature_id", $feature->getId());
        $moneyFunded = $this->purchaseTable->getMoneyRaised("feature_id", $feature->getId());
        $nbParticipants = $this->pendingPurchaseTable->countParticipants("feature_id", $request->getAttribute("id"));
        $nbParticipantsFunded = $this->purchaseTable->countParticipants("feature_id", $request->getAttribute("id"));
        $comments = $this->commentsTable->findListWithChildrenByFeature($id, true);
        $stripeUserId = $this->userTable->find($game->getUserId())->getStripeUserId();
        $tags = $this->featureTagTable->findAllByFeature($feature->getId());

        if ($this->auth->getUser()) {
            $userId = $this->auth->getUser()->getId();
        } else {
            $userId = "";
        }
        $voteExist = ($this->votesTable->findVote($userId, $feature->getId())->fetch() === false) ? false : true;

        if ($feature->getSlug() !== $slug) {
            return $this->redirect("blog.feature.showOneFeature", [
                        "slug" => $feature->getSlug(),
                        "id" => $feature->getId()
            ]);
        }
        return $this->renderer->render("@blog/showOneFeature", [
                    "feature" => $feature,
                    "status" => $status,
                    "moneyRaised" => $moneyRaised,
                    "nbParticipants" => $nbParticipants,
                    "moneyFunded" => $moneyFunded,
                    "nbParticipantsFunded" => $nbParticipantsFunded,
                    "votes" => $votes,
                    "votesYes" => $votesYes,
                    "votesNo" => $votesNo,
                    "voteExist" => $voteExist,
                    "userId" => $userId,
                    "game" => $game,
                    "comments" => $comments,
                    "stripeUserId" => $stripeUserId,
                    "tags" => $tags
        ]);
    }

}
