<?php

namespace App\Blog\Actions;

use App\Blog\Table\PlatformTable;
use App\Blog\Table\LicenceTable;
use App\Blog\Table\GameTable;
use App\Blog\Table\FeatureTable;
use Framework\Actions\RouterAwareAction;
use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class SearchShowAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var GameTable
     */
    private $gameTable;

    /**
     * @var FeatureTable
     */
    private $featureTable;

    /**
     * @var LicenceTable
     */
    private $licenceTable;

    /**
     * @var PlatformTable
     */
    private $platformTable;

    protected $routePrefix = "blog.index";

    use RouterAwareAction;

    public function __construct(
            RendererInterface $renderer, GameTable $gameTable, FeatureTable $featureTable, LicenceTable $licenceTable, PlatformTable $platformTable
    ) {
        $this->renderer = $renderer;
        $this->gameTable = $gameTable;
        $this->featureTable = $featureTable;
        $this->licenceTable = $licenceTable;
        $this->platformTable = $platformTable;
    }

    public function __invoke(Request $request) {
        $params = $request->getQueryParams();
        $inputSearch = $request->getAttribute("search");

        //var_dump($game);die();
        //findPaginated(12): pagination réglée à 12
        $games = $this->gameTable->findLike("name", $inputSearch)->paginate(12, $params["p"] ?? 1, $inputSearch);

        $licences = $this->licenceTable->findAll();
        $platforms = $this->platformTable->findAll();
        $liens_platforms = $this->gameTable->findAllPlatform();
        $features = $this->featureTable->findAll();
        $this->renderer->addGlobal("routePrefix", $this->routePrefix);
        $lien = "null";
        $page = $params["p"] ?? 1;
        return $this->renderer->render("@blog/index", compact("games", "inputSearch", "liens_platforms", "lien", "filter", "licences", "licence", "platforms", "platform", "platforms_filter", "licences_filter", "slug", "page", "features"));
    }

}
