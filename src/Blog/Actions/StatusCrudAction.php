<?php
namespace App\Blog\Actions;

use App\Blog\Table\StatusTable;
use Framework\Actions\CrudAction;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Framework\Session\FlashService;
use Psr\Http\Message\ServerRequestInterface;

class StatusCrudAction extends CrudAction
{
    
    protected $viewPath= "@blog/admin/status";
    
    protected $routePrefix= "blog.status.admin";
    
    protected $acceptedParams = ['name', 'slug'];
    
    public function __construct(
        RendererInterface $renderer,
        Router $router,
        StatusTable $table,
        FlashService $flash
    ) {
        parent::__construct($renderer, $router, $table, $flash);
    }
    
  
    protected function getValidator(ServerRequestInterface $request)
    {
        return parent::getValidator($request)
            ->required("name", "slug")
            ->length("name", 2, 250)
            ->length("slug", 2, 50)
            ->unique("slug", $this->table->getTable(), $this->table->getPdo(), $request->getAttribute("id"))
            ->slug("slug");
    }
}
