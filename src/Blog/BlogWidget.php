<?php
namespace App\Blog;

use App\Admin\AdminWidgetInterface;
use App\Blog\Table\GameTable;
use Framework\Renderer\RendererInterface;

class BlogWidget implements AdminWidgetInterface
{

    /**
     * @var RendererInterface
     */
    private $renderer;
    /**
     * @var GameTable
     */
    private $gameTable;
    
    public function __construct(RendererInterface $renderer, GameTable $gameTable)
    {
        $this->renderer= $renderer;
        $this->gameTable = $gameTable;
    }
    
    public function render(): string
    {
        $count= $this->gameTable->count();
        return $this->renderer->render("@blog/admin/widget", compact("count"));
    }

    public function renderMenu(): string
    {
        return $this->renderer->render("@blog/admin/menu");
    }
}
