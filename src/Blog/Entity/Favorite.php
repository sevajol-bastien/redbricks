<?php

namespace App\Blog\Entity;

class Favorite {

    /**
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var int
     */
    protected $userId;

    /**
     *
     * @var int
     */
    protected $gameId;

    /**
     * 
     * @return int
     */
    function getId(): int {
        return $this->id;
    }

    /**
     * 
     * @return int
     */
    function getUserId(): int {
        return $this->userId;
    }

    /**
     * 
     * @return int
     */
    function getGameId(): int {
        return $this->gameId;
    }

    /**
     * 
     * @param int $id
     */
    function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @param int $userId
     */
    function setUserId(int $userId) {
        $this->userId = $userId;
    }

    /**
     * 
     * @param int $gameId
     */
    function setGameId(int $gameId) {
        $this->gameId = $gameId;
    }

}
