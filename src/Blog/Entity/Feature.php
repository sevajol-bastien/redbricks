<?php

namespace App\Blog\Entity;

use Framework\Entity\Timestamp;

class Feature {

    /**
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $slug;

    /**
     *
     * @var int
     */
    protected $projectId;

    /**
     *
     * @var int
     */
    protected $statusId;

    /**
     *
     * @var int|null
     */
    protected $goal;

    /**
     *
     * @var \DateTime|null
     */
    protected $deadline;

    /**
     *
     * @var \DateTime|null
     */
    protected $estimatedDate;

    /**
     *
     * @var \DateTime|null
     */
    protected $deliveryDate;

    /**
     *
     * @var string|null
     */
    protected $image;

    /**
     *
     * @var \DateTime|null
     */
    protected $abandonDate;

    /**
     *
     * @var string|null
     */
    protected $abandonReason;

    /**
     *
     * @var int
     */
    protected $archived;

    /**
     *
     * @var int
     */
    protected $userId;

    /**
     *
     * @var string
     */
    protected $author;

    /**
     *
     * @var string
     */
    protected $content;

    use Timestamp;

    /**
     * 
     * @return int
     */
    public function getUserId(): int {
        return $this->userId;
    }

    /**
     * 
     * @return string|null
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * 
     * @return string|null
     */
    public function getGoal(): ?string {
        return $this->goal;
    }

    /**
     * 
     * @return int
     */
    public function getProjectId(): int {
        return $this->projectId;
    }

    /**
     * 
     * @return string
     */
    public function getThumb(): string {
        ['filename' => $filename, 'extension' => $extension] = pathinfo($this->image);
        return '/uploads/' . $this->projectId . '/features/' . $filename . '_thumb.' . $extension;
    }

    /**
     * 
     * @return string
     */
    public function getIllustration(): string {
        ['filename' => $filename, 'extension' => $extension] = pathinfo($this->image);
        return '/uploads/' . $this->projectId . '/features/' . $filename . '_illustration.' . $extension;
    }

    /**
     * 
     * @return string
     */
    public function getImageUrl(): string {
        return '/uploads/' . $this->projectId . '/features/' . $this->image;
    }

    /**
     * @param \DateTime|string|null $datetime
     */
    public function setDeadline($datetime): void {
        if (empty($datetime)) {
            $this->deadline = null;
        } else if (is_string($datetime)) {
            $this->deadline = new \DateTime($datetime);
        } else {
            $this->deadline = $datetime;
        }
    }

    /**
     * @param \DateTime|string|null $datetime
     */
    public function setEstimatedDate($datetime): void {
        if (empty($datetime)) {
            $this->estimatedDate = null;
        } else if (is_string($datetime)) {
            $this->estimatedDate = new \DateTime($datetime);
        } else {
            $this->estimatedDate = $datetime;
        }
    }

    /**
     * @param \DateTime|string|null $datetime
     */
    public function setDeliveryDate($datetime): void {
        if (empty($datetime)) {
            $this->deliveryDate = null;
        } else if (is_string($datetime)) {
            $this->deliveryDate = new \DateTime($datetime);
        } else {
            $this->deliveryDate = $datetime;
        }
    }

    /**
     * @param \DateTime|string|null $datetime
     */
    public function setAbandonDate($datetime): void {
        if (empty($datetime)) {
            $this->abandonDate = null;
        } else if (is_string($datetime)) {
            $this->abandonDate = new \DateTime($datetime);
        } else {
            $this->abandonDate = $datetime;
        }
    }

    /**
     * 
     * @return string
     */
    public function getAuthor(): string {
        return $this->author;
    }

    /**
     * 
     * @param string $author
     */
    public function setAuthor(string $author) {
        $this->author = $author;
    }

    /**
     * 
     * @return int|null
     */
    function getId(): ?int {
        return $this->id;
    }

    /**
     * 
     * @return string
     */
    function getSlug(): string {
        return $this->slug;
    }

    /**
     * 
     * @return int|null
     */
    function getStatusId(): ?int {
        return $this->statusId;
    }

    /**
     * 
     * @return \DateTime|null
     */
    function getDeadline(): ?\DateTime {
        return $this->deadline;
    }

    /**
     * 
     * @return \DateTime|null
     */
    function getEstimatedDate(): ?\DateTime {
        return $this->estimatedDate;
    }

    /**
     * 
     * @return \DateTime|null
     */
    function getDeliveryDate(): ?\DateTime {
        return $this->deliveryDate;
    }

    /**
     * 
     * @return string|null
     */
    function getImage(): ?string {
        return $this->image;
    }

    /**
     * 
     * @return \DateTime|null
     */
    function getAbandonDate(): ?\DateTime {
        return $this->abandonDate;
    }

    /**
     * 
     * @return string|null
     */
    function getAbandonReason(): ?string {
        return $this->abandonReason;
    }

    /**
     * 
     * @param int $id
     */
    function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @param string $name
     */
    function setName(string $name) {
        $this->name = $name;
    }

    /**
     * 
     * @param string $slug
     */
    function setSlug(string $slug) {
        $this->slug = $slug;
    }

    /**
     * 
     * @param int $projectId
     */
    function setProjectId(int $projectId) {
        $this->projectId = $projectId;
    }

    /**
     * 
     * @param int $statusId
     */
    function setStatusId(int $statusId) {
        $this->statusId = $statusId;
    }

    /**
     * 
     * @param string|null $goal
     */
    function setGoal(?string $goal) {
        $this->goal = $goal;
    }

    /**
     * 
     * @param string|null $image
     */
    function setImage(?string $image) {
        $this->image = $image;
    }

    /**
     * 
     * @param string|null $abandonReason
     */
    function setAbandonReason(?string $abandonReason) {
        $this->abandonReason = $abandonReason;
    }

    /**
     * 
     * @param int $userId
     */
    function setUserId(int $userId) {
        $this->userId = $userId;
    }

    /**
     * 
     * @return string|null
     */
    function getContent(): ?string {
        return $this->content;
    }

    /**
     * 
     * @param string $content
     */
    function setContent(string $content) {
        $this->content = $content;
    }

    /**
     * 
     * @param int $archived
     */
    function setArchived(int $archived) {
        $this->archived = $archived;
    }

    /**
     * 
     * @return int
     */
    function getArchived(): int {
        return $this->archived;
    }

    /**
     * 
     * @return bool
     */
    function isArchived(): bool {
        return $this->archived == 1;
    }

}
