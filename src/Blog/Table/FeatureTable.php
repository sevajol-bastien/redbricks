<?php

namespace App\Blog\Table;

use App\Blog\Entity\Feature;
use Framework\Database\Query;
use Framework\Database\Table;
use App\Auth\UserTable;
use App\Blog\Table\StatusTable;
use App\Blog\Table\FeatureContentTable;
use App\Blog\Table\ContributorTable;

class FeatureTable extends Table {

    protected $entity = Feature::class;
    protected $table = "features";

    public function findAll(): Query {
        $langcode = getenv("LANG");
        return $this->findAllCommon($langcode);
    }

    public function findAllEn(): Query {
        return $this->findAllCommon('en');
    }

    private function findAllCommon(string $langcode): Query {
        $user = new UserTable($this->pdo);
        $status = new StatusTable($this->pdo);
        $featureContent = new FeatureContentTable($this->pdo);
        $statusName = "s.name_" . $langcode;
        return $this->makeQuery()
                        ->select("f.*, u.displayname as author, $statusName as statusName, s.slug as statusSlug, fc.content as content, fc.name as name")
                        ->join($user->getTable() . " as u", "u.id = f.user_id")
                        ->join($status->getTable() . " as s", "s.id = f.status_id")
                        ->join($featureContent->getTable() . " as fc", "fc.feature_id = f.id")
                        ->where("fc.langcode = '$langcode' OR (fc.langcode = 'en' AND f.id NOT IN "
                                . "(SELECT fc.feature_id FROM {$featureContent->getTable()} as fc WHERE fc.langcode = '$langcode'))")
                        ->order("f.archived, f.updated_at DESC");
    }

    public function findShow(int $gameId): Query {
        //var_dump($this->findAll()->where("f.project_id = $gameId")->fetch());die();
        return $this->findAll()->where("f.project_id = $gameId");
    }

    public function findOneShow(int $id): Feature {
        //var_dump($this->findPublic()->where("g.id = $gameId")->fetch());die();
        return $this->findAll()->where("f.id = $id")->fetch();
    }

    public function findShowFeature(int $gameId, string $slug): Feature {
        //var_dump($gameId, $slug);die();
        return $this->findShow($gameId)->where("f.slug = '" . $slug . "'")->fetch();
    }

    public function countAndGroupBy(string $field): self {
        $query = clone $this;
        //var_dump($this->makeQuery()->select("COUNT(*)")-> groupBy($field));die();
        return $this->makeQuery()->select("COUNT(*)")->groupBy($field)->fetchColumn();
    }

    public function findAllUser(int $id): Query {
        $contributor = new ContributorTable($this->pdo);
        return $this->makeQuery()
                        ->select("f.project_id")
                        ->join($contributor->getTable() . " as c", "c.game_id = f.project_id")
                        ->order("f.created_at DESC")
                        ->where("f.user_id = $id or c.user_id = $id");
    }

    public function find($id) {
        return $this->findAll()->where("f.id = $id")->fetchOrFail();
    }

    public function findEn(int $id) {
        return $this->findAllEn()->where("f.id = $id")->fetchOrFail();
    }

}
