<?php

namespace App\Blog\Table;

use Framework\Database\Table;
use App\Blog\Entity\FeatureTag;
use App\Blog\Table\TagTable;
use Framework\Database\Query;

class FeatureTagTable extends Table {

    protected $entity = FeatureTag::class;
    protected $table = "features_tag";

    /**
     * 
     * @return Query
     */
    public function findAll(): Query {
        $name = 't.name_' . getenv("LANG");
        $tag = new TagTable($this->pdo);
        return $this->makeQuery()
                        ->select("f.id, $name as name, t.color, f.feature_id, f.tag_id")
                        ->join($tag->getTable() . ' as t', "t.id = f.tag_id");
    }

    /**
     * 
     * @param int $featureId
     * @return Query
     */
    public function findAllByFeature(int $featureId): Query {
        return $this->findAll()
                        ->where("f.feature_id = $featureId");
    }

    /**
     * 
     * @param int $featureId
     * @return array
     */
    public function findListByFeature(int $featureId): array {
        $results = $this->findAll()
                        ->where("f.feature_id = $featureId")
                        ->fetchAll()
                ->records;
        $list = [];
        //echo '<pre>'; var_dump($results); echo '</pre>'; die();
        foreach ($results as $result) {
            $list[$result['tag_id']] = $result['name'];
        }
        //echo '<pre>'; var_dump($list); echo '</pre>'; die();
        return $list;
    }

    public function find(int $id) {
        return $this->findAll()
                        ->where("f.id = $id")
                        ->fetchOrFail();
    }

    /**
     * 
     * @param int $featureId
     * @param int $tagId
     * @return bool
     */
    public function deleteByTag(int $featureId, int $tagId): bool {
        $query = $this->pdo->prepare("DELETE FROM {$this->table} WHERE feature_id=? AND tag_id=?");
        return $query->execute([$featureId, $tagId]);
    }

}
