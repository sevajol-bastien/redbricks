<?php

namespace App\Blog\Table;

use Framework\Database\Table;
use App\Blog\Entity\GameContent;
use App\Blog\Table\GameTable;
use App\Localization\Table\LangTable;
use Framework\Database\Query;

class GameContentTable extends Table {

    protected $entity = GameContent::class;
    protected $table = "games_content";

    public function findAll(): Query {
        $game = new GameTable($this->pdo);
        $lang = new LangTable($this->pdo);
        return $this->makeQuery()
                        ->select("g.*, game.name as title, l.name as language")
                        ->join($game->getTable() . " as game", "game.id = g.game_id")
                        ->join($lang->getTable() . " as l", "l.code = g.langcode");
    }

    public function findAllByGame(int $gameId): Query {
        return $this->findAll()
                        ->where("g.game_id = $gameId")
                        ->order("g.langcode");
    }

    public function find(int $id) {
        return $this->findAll()
                        ->where("g.id = $id")
                        ->fetchOrFail();
    }
    
    public function updateByGame(int $id, array $params): bool {
        $fieldQuery = $this->buildFieldQuery($params);
        $params["game_id"] = $id;
        $query = $this->pdo->prepare("UPDATE {$this->table} SET $fieldQuery WHERE game_id= :game_id AND langcode = 'en'");
        return $query->execute($params);
    }
}
