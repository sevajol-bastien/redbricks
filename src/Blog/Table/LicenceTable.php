<?php

namespace App\Blog\Table;

use Framework\Database\Table;
use Framework\Database\Query;

class LicenceTable extends Table {

    protected $table = "licences";

    public function findAll(): Query {
        $name = 'l.name_' . getenv("LANG");
        return $this->makeQuery()
                        ->select("l.id, $name as name, l.slug");
    }

}
