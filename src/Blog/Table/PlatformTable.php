<?php

namespace App\Blog\Table;

use Framework\Database\Table;
use Framework\Database\Query;

class PlatformTable extends Table {

    protected $table = "platforms";

    public function findAll(): Query {
        $name = 'name_' . getenv("LANG");
        return $this->makeQuery()
                        ->select("id, $name as name, slug");
    }

}
