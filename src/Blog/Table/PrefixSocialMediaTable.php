<?php

namespace App\Blog\Table;

use Framework\Database\Table;
use Framework\Database\Query;

class PrefixSocialMediaTable extends Table {

    protected $table = "prefix_social_media";

    public function findAll(): Query {
        return $this->makeQuery()
                        ->order("p.id");
    }

}
