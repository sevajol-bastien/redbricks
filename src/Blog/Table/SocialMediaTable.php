<?php

namespace App\Blog\Table;

use Framework\Database\Table;
use App\Blog\Table\PrefixSocialMediaTable;

class SocialMediaTable extends Table {

    protected $table = "social_media";

    public function findByGame(int $game_id) {
        return $this->findAll()
                        ->where("s.project_id = $game_id");
    }

    public function findCompletedByGame(int $game_id) {
        $prefix = new PrefixSocialMediaTable($this->pdo);
        return $this->findAll()
                        ->select("s.*, p.address as preAddress, p.media as name, p.icon as icon")
                        ->join($prefix->getTable() . " as p", "p.id=s.media_id")
                        ->where("s.project_id = $game_id")
                        ->order("s.media_id");
    }

}
