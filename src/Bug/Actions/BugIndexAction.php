<?php

namespace App\Bug\Actions;

use App\Bug\Table\BugTable;
use App\Blog\Table\GameTable;
use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class BugIndexAction {

    public function __construct(
    RendererInterface $renderer, GameTable $gameTable, BugTable $bugTable
    ) {

        $this->renderer = $renderer;
        $this->gameTable = $gameTable;
        $this->bugTable = $bugTable;
    }

    public function __invoke(Request $request) {
        $id = $request->getAttribute("id");
        $game = $this->gameTable->findShow($id);
        $bugs = $this->bugTable->findShow($id);
        return $this->renderer->render("@bug/index", compact("game", "bugs"));
    }

}
