<?php

namespace App\Bug\Actions;

use Framework\Renderer\RendererInterface;
use Framework\Response\RedirectResponse;
use Psr\Http\Message\ServerRequestInterface;
use Framework\Router;
use App\Bug\Table\BugTable;
use App\Blog\Table\GameTable;
use App\Bug\Table\StatusBugTable;
use App\Bug\Table\SeverityTable;
use App\Bug\Table\FrequencyTable;
use App\Blog\Table\CategoryTable;
use App\Blog\Table\FeatureTable;
use App\Comments\Table\CommentsTable;
use App\News\Table\NewsTable;
use App\Survey\Table\SurveyTable;
use Framework\Validator;
use Framework\Auth;
use App\Bug\Entity\Bug;
use Framework\Database\Hydrator;
use Framework\Session\FlashService;

class BugListAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     *
     * @var GameTable
     */
    protected $gameTable;

    /**
     *
     * @var BugTable
     */
    protected $bugTable;

    /**
     *
     * @var StatusBugTable
     */
    protected $statusBugTable;

    /**
     *
     * @var SeverityTable
     */
    protected $severityTable;

    /**
     *
     * @var CategoryTable
     */
    protected $categoryTable;

    /**
     *
     * @var FrequencyTable
     */
    protected $frequencyTable;

    /**
     *
     * @var FeatureTable
     */
    protected $featureTable;

    /**
     *
     * @var CommentsTable
     */
    protected $commentsTable;

    /**
     *
     * @var NewsTable
     */
    protected $newsTable;

    /**
     *
     * @var SurveyTable
     */
    protected $surveyTable;

    /**
     *
     * @var Auth
     */
    protected $auth;

    /**
     *
     * @var FlashService
     */
    protected $flash;

    /**
     *
     * @var Bug
     */
    protected $item;

    public function __construct(
            RendererInterface $renderer, Router $router, GameTable $gameTable, BugTable $bugTable, StatusBugTable $statusBugTable, SeverityTable $severityTable, CategoryTable $categoryTable, FrequencyTable $frequencyTable, FeatureTable $featureTable, CommentsTable $commentsTable, NewsTable $newsTable, SurveyTable $surveyTable, Auth $auth, FlashService $flash
    ) {
        $this->renderer = $renderer;
        $this->router = $router;
        $this->gameTable = $gameTable;
        $this->bugTable = $bugTable;
        $this->statusBugTable = $statusBugTable;
        $this->categoryTable = $categoryTable;
        $this->severityTable = $severityTable;
        $this->frequencyTable = $frequencyTable;
        $this->featureTable = $featureTable;
        $this->commentsTable = $commentsTable;
        $this->newsTable = $newsTable;
        $this->surveyTable = $surveyTable;
        $this->auth = $auth;
        $this->flash = $flash;
    }

    /**
     * @param ServerRequestInterface $request
     * @return RedirectResponse|string
     */
    public function __invoke(ServerRequestInterface $request) {
        $slug = $request->getAttribute("slug");
        $gameId = $request->getAttribute("id");
        $game = $this->gameTable->findShow($gameId);
        if ($request->getMethod() === "POST") {
            $errors = $this->addBug($request);
            $item = $this->item;
        }

        $bugs = $this->bugTable->findShow($gameId);
        $statusBugs = $this->statusBugTable->findAll();
        $category = $this->categoryTable->findAll();
        $severity = $this->severityTable->findAll();
        $frequency = $this->frequencyTable->findAll();

        $nbFeatures = $this->featureTable->findShow($gameId)->where("s.id>1")->count("*");
        $nbBugs = $bugs->count("*");
        $nbComments = $this->commentsTable->findCount($gameId);
        $nbNews = $this->newsTable->findShow($gameId)->count("*");
        $nbSurveys = $this->surveyTable->findShow($gameId)->count("*");

        foreach ($statusBugs->fetchColumn() as $numStatus) {
            $nbBugsSort[$numStatus] = $this->bugTable->findAll()->where("b.status_id = $numStatus", "b.project_id=$gameId")->count('*');
        }
        $nbBugsSort[0] = $nbBugs;

        if ($game->getSlug() !== $slug) {
            return $this->redirect("blog.show", [
                        "slug" => $game->getSlug(),
                        "id" => $game->getId()
            ]);
        }

        return $this->renderer->render('@bug/list', compact("game", "bugs", "nbBugsSort", "statusBugs", "frequency", "severity", "category", "nbFeatures", "nbBugs", "nbComments", "nbNews", "nbSurveys", "errors", "item"));
    }

    private function addBug(ServerRequestInterface $request) {
        $this->item = new Bug();
        $this->item->setCreatedAt(new \DateTime());
        $validator = new Validator($request->getParsedBody());
        $validator->required("title", "description")
                ->length("title", 2, 50)
                ->length("description", 10);
        if ($validator->isValid() && $this->auth->getUser() != null) {
            $gameId = $request->getAttribute("id");
            $params = $request->getParsedBody();
            $this->bugTable->insertUnique([
                "project_id" => $gameId,
                "status_id" => 1,
                "title" => $params['title'],
                "description" => $params['description'],
                "severity_id" => 0,
                "category_id" => 1,
                "frequency_id" => 0,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s"),
                "user_id" => $this->auth->getUser()->getId()
            ]);
            if ($this->bugTable->getPdo()->lastInsertId() == 0) {
                $this->flash->error(_("The fast bug report could not be saved. A bug report has already been recorded under this title."));
                Hydrator::hydrate($request->getParsedBody(), $this->item);
            } else {
                $this->flash->success(_("The fast bug report was saved."));
            }
        } else {
            $this->flash->error(_("The fast bug report could not be saved."));
            Hydrator::hydrate($request->getParsedBody(), $this->item);
        }
        return $validator->getErrors();
    }

}
