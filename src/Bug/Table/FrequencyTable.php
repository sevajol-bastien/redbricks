<?php

namespace App\Bug\Table;

use Framework\Database\Table;
use Framework\Database\Query;

class FrequencyTable extends Table {

    protected $table = "frequency";

    /**
     * 
     * @return array
     */
    public function findList(): array {
        $name = "name_" . getenv("LANG");
        $results = $this->pdo
                ->query("SELECT id, $name as name FROM {$this->table} ORDER BY id")
                ->fetchAll(\PDO::FETCH_NUM);
        $list = [];
        foreach ($results as $result) {
            $list[$result[0]] = $result[1];
        }
        return $list;
    }

    public function findAll(): Query {
        $name = 'f.name_' . getenv("LANG");
        return $this->makeQuery()
                        ->select("f.id, $name as name, f.slug");
    }

}
