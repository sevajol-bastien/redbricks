<?php

namespace App\Bug\Table;

use Framework\Database\Table;
use Framework\Database\Query;

class SeverityTable extends Table {

    protected $table = "severity";

    /**
     * 
     * @return array
     */
    public function findList(): array {
        $name = "name_" . getenv("LANG");
        $results = $this->pdo
                ->query("SELECT id, $name as name FROM {$this->table} ORDER BY id")
                ->fetchAll(\PDO::FETCH_NUM);
        $list = [];
        foreach ($results as $result) {
            $list[$result[0]] = $result[1];
        }
        return $list;
    }

    public function findAll(): Query {
        $name = 's.name_' . getenv("LANG");
        return $this->makeQuery()
                        ->select("s.id, $name as name, s.slug");
    }

}
