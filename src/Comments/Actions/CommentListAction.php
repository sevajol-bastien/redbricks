<?php

namespace App\Comments\Actions;

use Framework\Renderer\RendererInterface;
use Framework\Response\RedirectResponse;
use Psr\Http\Message\ServerRequestInterface;
use Framework\Router;
use App\Blog\Table\GameTable;
use App\Comments\Table\CommentsTable;
use App\Blog\Table\FeatureTable;
use App\News\Table\NewsTable;
use App\Bug\Table\BugTable;
use App\Survey\Table\SurveyTable;

class CommentListAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    public function __construct(
            RendererInterface $renderer, Router $router, GameTable $gameTable, CommentsTable $commentsTable, FeatureTable $featureTable, BugTable $bugTable, NewsTable $newsTable, SurveyTable $surveyTable
    ) {
        $this->renderer = $renderer;
        $this->router = $router;
        $this->gameTable = $gameTable;
        $this->commentsTable = $commentsTable;
        $this->featureTable = $featureTable;
        $this->newsTable = $newsTable;
        $this->bugTable = $bugTable;
        $this->surveyTable = $surveyTable;
    }

    /**
     * @param ServerRequestInterface $request
     * @return RedirectResponse|string
     */
    public function __invoke(ServerRequestInterface $request) {
        $slug = $request->getAttribute("slug");
        $gameId = $request->getAttribute("id");
        $game = $this->gameTable->findShow($gameId);
        $comments = $this->commentsTable->findListWithChildrenByGame($gameId, true);

        $nbFeatures = $this->featureTable->findShow($gameId)->where("s.id>1")->count("*");
        $nbBugs = $this->bugTable->findShow($gameId)->count("*");
        $nbComments = $this->commentsTable->findCount($gameId);
        $nbNews = $this->newsTable->findShow($gameId)->count("*");
        $nbSurveys = $this->surveyTable->findShow($gameId)->count("*");

        if ($game->getSlug() !== $slug) {
            return $this->redirect("blog.show", [
                        "slug" => $game->getSlug(),
                        "id" => $game->getId()
            ]);
        }

        return $this->renderer->render('@comments/listComments', [
                    "game" => $game,
                    "comments" => $comments,
                    "nbFeatures" => $nbFeatures,
                    "nbBugs" => $nbBugs,
                    "nbComments" => $nbComments,
                    "nbNews" => $nbNews,
                    "nbSurveys" => $nbSurveys
        ]);
    }

}
