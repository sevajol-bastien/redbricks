<?php

namespace App\Comments;

class Comment {

    /**
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var int
     */
    protected $parentId;

    /**
     *
     * @var int
     */
    protected $userId;

    /**
     *
     * @var string
     */
    protected $author;

    /**
     *
     * @var string
     */
    protected $content;

    /**
     *
     * @var int
     */
    protected $gameId;

    /**
     *
     * @var int
     */
    protected $featureId;

    /**
     *
     * @var int
     */
    protected $bugId;
    
    /**
     *
     * @var int
     */
    protected $newsId;

    /**
     *
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * 
     * @return int
     */
    function getId(): int {
        return $this->id;
    }

    /**
     * 
     * @return int
     */
    function getParentId(): int {
        return $this->parentId;
    }

    /**
     * 
     * @return int
     */
    function getUserId(): int {
        return $this->userId;
    }

    /**
     * 
     * @return string
     */
    function getAuthor(): string {
        return $this->author;
    }

    /**
     * 
     * @return string
     */
    function getContent(): string {
        return $this->content;
    }

    /**
     * 
     * @return int
     */
    function getGameId(): int {
        return $this->gameId;
    }

    /**
     * 
     * @return int
     */
    function getFeatureId(): int {
        return $this->featureId;
    }

    /**
     * 
     * @return int
     */
    function getBugId(): int {
        return $this->bugId;
    }

    /**
     * 
     * @return \DateTime
     */
    function getCreatedAt(): \DateTime {
        return $this->createdAt;
    }

    /**
     * 
     * @param int $id
     */
    function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @param int $parentId
     */
    function setParentId(int $parentId) {
        $this->parentId = $parentId;
    }

    /**
     * 
     * @param int $userId
     */
    function setUserId(int $userId) {
        $this->userId = $userId;
    }

    /**
     * 
     * @param string $author
     */
    function setAuthor(string $author) {
        $this->author = $author;
    }

    /**
     * 
     * @param string $content
     */
    function setContent(string $content) {
        $this->content = $content;
    }

    /**
     * 
     * @param int $gameId
     */
    function setGameId(int $gameId) {
        $this->gameId = $gameId;
    }

    /**
     * 
     * @param int $featureId
     */
    function setFeatureId(int $featureId) {
        $this->featureId = $featureId;
    }

    /**
     * 
     * @param int $bugId
     */
    function setBugId(int $bugId) {
        $this->bugId = $bugId;
    }

    /**
     * 
     * @param string|\DateTime $createdAt
     */
    function setCreatedAt($createdAt) {
        if (is_string($createdAt)) {
            $this->createdAt = new \DateTime($createdAt);
        } else {
            $this->createdAt = $createdAt;
        }
    }
    
    /**
     * 
     * @return int
     */
    function getNewsId(): int {
        return $this->newsId;
    }

    /**
     * 
     * @param int $newsId
     */
    function setNewsId(int $newsId) {
        $this->newsId = $newsId;
    }



}
