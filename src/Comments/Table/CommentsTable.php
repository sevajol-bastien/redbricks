<?php

namespace App\Comments\Table;

use Framework\Database\Table;
use App\Auth\UserTable;
use Framework\Database\Query;
use App\Comments\Comment;
use App\Blog\Table\FeatureContentTable;
use App\Blog\Table\FeatureTable;
use App\Bug\Table\BugTable;
use App\News\Table\NewsTable;

class CommentsTable extends Table {

    protected $entity = Comment::class;
    protected $table = "comments";

    /**
     * 
     * @param int $id
     * @return bool|mixed
     * @throws NoRecordException
     */
    public function find(int $id) {
        return $this->findAll()->where("c.id = $id")->fetchOrFail();
    }

    /**
     * 
     * @param int $gameId
     * @return int
     */
    public function findCount(int $gameId): int {
        return $this->makeQuery()
                        ->where("c.game_id = $gameId")
                        ->count();
    }

    /**
     * 
     * @return Query
     */
    public function findAll(): Query {
        $langcode = getenv("LANG");
        $featureContent = new FeatureContentTable($this->pdo);
        $feature = new FeatureTable($this->pdo);
        $user = new UserTable($this->pdo);
        $bug = new BugTable($this->pdo);
        $news = new NewsTable($this->pdo);
        return $this->makeQuery()
                        ->select("c.*, u.displayname as author, u.active, fc.name as featureName, f.slug as featureSlug, b.title as bugTitle, n.title as newsTitle")
                        ->join($featureContent->getTable() . " as fc", "fc.feature_id = c.feature_id")
                        ->join($user->getTable() . " as u", "u.id = c.user_id")
                        ->join($bug->getTable() . " as b", "b.id = c.bug_id")
                        ->join($news->getTable() . " as n", "n.id = c.news_id")
                        ->join($feature->getTable() . " as f", "f.id = c.feature_id")
                        ->where("c.feature_id = 0 OR (fc.langcode = '$langcode' OR (fc.langcode = 'en' AND fc.feature_id NOT IN "
                                . "(SELECT fc.feature_id FROM {$featureContent->getTable()} as fc WHERE fc.langcode = '$langcode')))");
    }

    /**
     * 
     * @param int $game_id
     * @return array
     */
    public function findListByGame(int $game_id): array {
        return $this->sortById($this->findAll()
                                ->where("c.game_id = $game_id")
                                ->fetchAll());
    }

    /**
     * 
     * @param int $bug_id
     * @return array
     */
    public function findListByBug(int $bug_id): array {
        return $this->sortById($this->findAll()
                                ->where("c.bug_id = $bug_id")
                                ->fetchAll());
    }

    /**
     * 
     * @param int $feature_id
     * @return array
     */
    public function findListByFeature(int $feature_id): array {
        return $this->sortById($this->findAll()
                                ->where("c.feature_id = $feature_id")
                                ->fetchAll());
    }

    /**
     * 
     * @param int $news_id
     * @return array
     */
    public function findListByNews(int $news_id): array {
        return $this->sortById($this->findAll()
                                ->where("c.news_id = $news_id")
                                ->fetchAll());
    }

    /**
     * 
     * @param array $comments
     * @return array
     */
    private function sortById($comments): array {
        $comments_by_id = [];
        foreach ($comments as $comment) {
            $comments_by_id[$comment->getId()] = $comment;
        }

        return $comments_by_id;
    }

    /**
     * 
     * @param Comment $a
     * @param Comment $b
     * @return bool
     */
    private function cmp(Comment $a, Comment $b): bool {
        return $a->updatedAt < $b->updatedAt;
    }

    /**
     * 
     * @param array $comments
     * @param Comment $comment
     */
    private function sortByDate(array $comments, Comment $comment) {
        $comments[$comment->getParentId()]->updatedAt = $comment->updatedAt;
        if ($comments[$comment->getParentId()]->getParentId() != 0) {
            $this->sortByDate($comments, $comments[$comment->getParentId()]);
        }
    }

    /**
     * 
     * @param int $game_id
     * @param bool $unset_children
     * @return array
     */
    public function findListWithChildrenByGame(int $game_id, bool $unset_children = true): array {
        $comments = $comments_by_id = $this->findListByGame($game_id);
        //$translate = new GoogleTranslate($langcode);
        foreach ($comments as $id => $comment) {
            //$comment->translate = $translate->trans($comment->getContent());
            //$comment->lang = $translate->getLastDetectedSource();
            //echo '<pre>';            var_dump($comment, $translate); echo "</pre>";
            $comment->updatedAt = $comment->getCreatedAt();
            if ($comment->getParentId() != 0) {
                $comments_by_id[$comment->getParentId()]->children[] = $comment;
                $this->sortByDate($comments_by_id, $comment);
                if ($unset_children) {
                    unset($comments[$id]);
                }
            }
        }
        uasort($comments, array($this, 'cmp'));
        return $comments;
    }

    /**
     * 
     * @param int $feature_id
     * @param bool $unset_children
     * @return array
     */
    public function findListWithChildrenByFeature(int $feature_id, bool $unset_children = true): array {
        $comments = $comments_by_id = $this->findListByFeature($feature_id);
        foreach ($comments as $id => $comment) {
            $comment->updatedAt = $comment->getCreatedAt();
            if ($comment->getParentId() != 0) {
                $comments_by_id[$comment->getParentId()]->children[] = $comment;
                $this->sortByDate($comments_by_id, $comment);
                if ($unset_children) {
                    unset($comments[$id]);
                }
            }
        }
        uasort($comments, array($this, 'cmp'));
        return $comments;
    }

    /**
     * 
     * @param int $bug_id
     * @param bool $unset_children
     * @return array
     */
    public function findListWithChildrenByBug(int $bug_id, bool $unset_children = true): array {
        $comments = $comments_by_id = $this->findListByBug($bug_id);
        foreach ($comments as $id => $comment) {
            $comment->updatedAt = $comment->getCreatedAt();
            if ($comment->getParentId() != 0) {
                $comments_by_id[$comment->getParentId()]->children[] = $comment;
                $this->sortByDate($comments_by_id, $comment);
                if ($unset_children) {
                    unset($comments[$id]);
                }
            }
        }
        uasort($comments, array($this, 'cmp'));
        return $comments;
    }

    /**
     * 
     * @param int $news_id
     * @param bool $unset_children
     * @return array
     */
    public function findListWithChildrenByNews(int $news_id, bool $unset_children = true): array {
        $comments = $comments_by_id = $this->findListByNews($news_id);
        foreach ($comments as $id => $comment) {
            $comment->updatedAt = $comment->getCreatedAt();
            if ($comment->getParentId() != 0) {
                $comments_by_id[$comment->getParentId()]->children[] = $comment;
                $this->sortByDate($comments_by_id, $comment);
                if ($unset_children) {
                    unset($comments[$id]);
                }
            }
        }
        uasort($comments, array($this, 'cmp'));
        return $comments;
    }

    /**
     * 
     * @param int $game_id
     * @param int $nb
     * @return Query
     */
    public function findLast(int $game_id, int $nb): Query {
        return $this->findAll()
                        ->limit($nb)
                        ->where("c.game_id = $game_id")
                        ->order("c.created_at DESC");
    }

    /**
     * 
     * @param int $parent_id
     * @param array $params
     * @return bool
     */
    public function updateByParent(int $parent_id, array $params): bool {
        $fieldQuery = $this->buildFieldQuery($params);
        $params["id"] = $parent_id;
        $query = $this->pdo->prepare("UPDATE {$this->table} SET $fieldQuery WHERE parent_id= :id");
        return $query->execute($params);
    }

    /**
     * 
     * @param string $field
     * @param string $value
     * @return Query
     */
    public function findBy(string $field, string $value): Query {
        return $this->makeQuery()->where("$field = :field")->params(["field" => $value]);
    }

}
