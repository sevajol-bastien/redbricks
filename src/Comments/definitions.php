<?php

return [
    'comment.from' => \DI\get('mail.from'),
    \App\Comments\Actions\CommentsAction::class => \DI\object()->constructorParameter('from', \DI\get('comment.from'))
];

