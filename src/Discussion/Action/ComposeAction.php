<?php

namespace App\Discussion\Action;

use App\Auth\UserTable;
use App\Discussion\Table\MessageTable;
use App\Discussion\Table\ThreadTable;
use App\Discussion\Table\ThreadUnreadTable;
use Framework\Actions\RouterAwareAction;
use Framework\Auth;
use Framework\Database\NoRecordException;
use Framework\Renderer\RendererInterface;
use Framework\Session\FlashService;
use Framework\Validator;
use Psr\Http\Message\ServerRequestInterface;
use App\Notification\SendNotification;
use Framework\Router;
use App\Discussion\Entity\Message;

class ComposeAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     *
     * @var Router
     */
    protected $router;

    /**
     * @var FlashService
     */
    private $flashService;

    /**
     * @var UserTable
     */
    private $userTable;

    /**
     * @var MessageTable
     */
    private $messageTable;

    /**
     * @var ThreadTable
     */
    private $threadTable;

    /**
     *
     * @var ThreadUnreadTable
     */
    protected $messageUnreadTable;

    /**
     * @var Auth
     */
    private $auth;

    /**
     *
     * @var SendNotification
     */
    protected $sendNotification;

    use RouterAwareAction;

    public function __construct(
            RendererInterface $renderer, Router $router, FlashService $flashService, UserTable $userTable, MessageTable $messageTable, ThreadTable $threadTable, ThreadUnreadTable $messageUnreadTable, Auth $auth, SendNotification $sendNotification
    ) {
        $this->renderer = $renderer;
        $this->router = $router;
        $this->flashService = $flashService;
        $this->userTable = $userTable;
        $this->messageTable = $messageTable;
        $this->threadTable = $threadTable;
        $this->messageUnreadTable = $messageUnreadTable;
        $this->auth = $auth;
        $this->sendNotification = $sendNotification;
    }

    public function __invoke(ServerRequestInterface $request) {
        if ($request->getMethod() === 'GET') {
            $idTo = $request->getAttribute('to');
            try {
                if ($idTo != 0) {
                    $to = $this->userTable->findBy('id', $idTo);
                }
                return $this->renderer->render('@discussion/compose', compact('to', 'message'));
            } catch (NoRecordException $exception) {
                $this->flashService->error(_("You can't send a message to this user"));
                return $this->redirect('blog.index');
            }
        }
        $params = $request->getParsedBody();
        $validator = (new Validator($params))
                ->required('to', 'message')
                ->notEmpty('message')
                ->notEmpty('to');
        if ($validator->isValid()) {
            $user = $this->auth->getUser();
            $array_to = explode(";", $params['to']);
            $array_users = [];
            foreach ($array_to as $to) {
                try {
                    $array_users[] = $this->userTable->findBy("username", trim($to))->getId();
                } catch (NoRecordException $exception) {
                    $this->flashService->error(_("One or more recipients don't exist"));
                    return $this->renderer->render('@discussion/compose', compact('to', 'message'));
                }
            }
            $array_users[] = $user->getId();
            asort($array_users);
            $id_users = implode(",", $array_users);
            try {
                $thread = $this->threadTable->findBy('id_users', $id_users);
                $id_thread = $thread->getId();
            } catch (NoRecordException $exception) {
                $threadParams = [
                    'id_users' => $id_users,
                    'last_message' => 0
                ];
                $this->threadTable->insert($threadParams);
                $id_thread = $this->threadTable->getPdo()->lastInsertId();
            }
            $messageParams = [
                'id_thread' => $id_thread,
                'id_from' => $user->getId(),
                'message' => $params['message'],
                'created_at' => date("Y-m-d H:i:s")
            ];
            $this->messageTable->insert($messageParams);
            $last_message = $this->messageTable->getPdo()->lastInsertId();
            $message = new Message();
            $message->setId($last_message);
            $message->setMessage($params['message']);
            $message->author = $user->getUsername();
            $message->setIdThread($id_thread);

            foreach ($array_users as $userId) {
                if ($userId != $user->getId()) {
                    $this->messageUnreadTable->insertUnique([
                        "thread_id" => $id_thread,
                        "user_id" => $userId
                    ]);
                    $this->sendNotification->send("mp", [
                        "message" => $message,
                        "userId" => $userId
                    ]);
                }
            }

            $this->threadTable->update($id_thread, ['last_message' => $last_message]);
            $this->flashService->success(_('Message send'));

            return $this->redirect('discussion.messages', ['idThread' => $id_thread]);
        } else {
            $this->flashService->error(_("Your message doesn't send"));
            $errors = $validator->getErrors();
            return $this->renderer->render('@discussion/compose', compact('errors', 'to', 'message'));
        }
    }

}
