<?php

namespace App\Discussion;

use App\Discussion\Action\ComposeAction;
use App\Discussion\Action\MessageViewAction;
use Framework\Auth\LoggedInMiddleware;
use Framework\Module;
use Framework\Renderer\RendererInterface;
use Framework\Router;

class DiscussionModule extends Module
{
    public function __construct(Router $router, RendererInterface $renderer)
    {
        $renderer->addPath('discussion', __DIR__ . '/views');
        $router->get('/compose/{to:[-0-9]+}', [LoggedInMiddleware::class, ComposeAction::class], 'discussion.compose');
        $router->post('/compose/{to:[-0-9]+}', [LoggedInMiddleware::class, ComposeAction::class]);
        $router->get('/messaging/{idThread:[-0-9]+}', [LoggedInMiddleware::class, MessageViewAction::class], 'discussion.messages');
        $router->post('/messaging/{idThread:[-0-9]+}', [LoggedInMiddleware::class, MessageViewAction::class]);
    }
}