<?php

namespace App\Discussion\Entity;

class Message {

    /**
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var int
     */
    protected $idThread;

    /**
     *
     * @var int
     */
    protected $idFrom;

    /**
     *
     * @var string
     */
    protected $message;

    /**
     *
     * @var \DateTime
     */
    protected $createdAt;

    /**
     *
     * @var int
     */
    protected $status;

    /**
     *
     * @var string|null
     */
    protected $avatar;

    /**
     * 
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * 
     * @return string
     */
    public function getMessage(): string {
        return $this->message;
    }

    /**
     * 
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime {
        return $this->createdAt;
    }

    /**
     * 
     * @return int
     */
    public function getStatus(): int {
        return $this->status;
    }

    /**
     * 
     * @return string|null
     */
    public function getAvatar(): ?string {
        return $this->avatar;
    }

    /**
     * @param string|null $avatar
     */
    public function setAvatar(?string $avatar) {
        $this->avatar = $avatar;
    }

    /**
     * 
     * @return string
     */
    public function getAvatarThumb(): string {
        ['filename' => $filename, 'extension' => $extension] = pathinfo($this->avatar);
        return '/uploads/avatars/' . $filename . '_thumb.' . $extension;
    }

    /**
     * 
     * @param string|null $avatar
     * @return string
     */
    public function setAvatarThumb(?string $avatar): string {
        $this->avatar = $avatar;
        return '/uploads/avatars/' . $this->avatar;
    }

    /**
     * 
     * @return string
     */
    public function getAvatarUrl(): string {
        return '/uploads/avatars/' . $this->avatar;
    }

    /**
     * 
     * @return int
     */
    function getIdThread(): int {
        return $this->idThread;
    }

    /**
     * 
     * @return int
     */
    function getIdFrom(): int {
        return $this->idFrom;
    }

    /**
     * 
     * @param int $idThread
     */
    function setIdThread(int $idThread) {
        $this->idThread = $idThread;
    }

    /**
     * 
     * @param int $idFrom
     */
    function setIdFrom(int $idFrom) {
        $this->idFrom = $idFrom;
    }

    /**
     * 
     * @param int $id
     */
    function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @param string $message
     */
    function setMessage(string $message) {
        $this->message = $message;
    }

    /**
     * 
     * @param \DateTime|string $createdAt
     */
    function setCreatedAt($datetime) {
        if (is_string($datetime)) {
            $this->createdAt = new \DateTime($datetime);
        } else {
            $this->createdAt = $datetime;
        }
    }

    /**
     * 
     * @param int $status
     */
    function setStatus(int $status) {
        $this->status = $status;
    }

}
