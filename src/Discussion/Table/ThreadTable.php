<?php

namespace App\Discussion\Table;

use Framework\Database\Table;
use App\Discussion\Table\MessageTable;
use App\Discussion\Entity\Thread;
use Framework\Database\Query;

class ThreadTable extends Table {

    protected $table = "threads";
    protected $entity = Thread::class;

    /**
     * 
     * @param int $id
     * @return Query
     */
    public function findAllUser(int $id): Query {
        $message = new MessageTable($this->pdo);
        return $this->findAll()
                        ->select("t.*, m.id_from, m.message, m.created_at")
                        ->join($message->getTable() . " as m", "m.id=t.last_message")
                        ->where("t.id_users = $id OR t.id_users LIKE '$id,%' OR t.id_users LIKE '%,$id,%' OR t.id_users LIKE '%,$id'")
                        ->order("m.created_at DESC");
    }

}
