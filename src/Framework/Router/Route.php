<?php
namespace Framework\Router;

/**
* Class Route
* Représente la route suivie
*/
class Route
{
    
    /**
    * @var string
    */
    private $name;
    
    /**
    * @var callable
    */
    private $callback;
    
    /**
    * @var array
    */
    private $parameters;
    
    /**
    * Route constructor
    * @param string $name
    * @param string/callable $callback
    * @param array $parameters
    */
    
    
    public function __construct(string $name, $callback, array $parameters)
    {
        $this->name= $name;
        $this->callback= $callback;
        $this->parameters= $parameters;
    }
    
    
    /**
    * @return string
    */
    public function getName():string
    {
        return $this->name;
    }
    /**
    * @return string/callable
    */
    public function getCallback()
    {
        return $this->callback;
    }
    /**
    * @return string[] $modules Liste des paramètres de l'URL
    */
    public function prePersist(): array
    {
        return $this->parameters;
    }
}
