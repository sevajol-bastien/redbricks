<?php

namespace App\Localization\Actions;

use Framework\Actions\RouterAwareAction;
use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Framework\Router;
use Framework\Auth;
use App\Auth\UserTable;

class LocalizationAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     *
     * @var Router
     */
    protected $router;
    protected $auth;
    protected $userTable;

    use RouterAwareAction;

    public function __construct(
            RendererInterface $renderer, Router $router, Auth $auth, UserTable $userTable
    ) {
        $this->renderer = $renderer;
        $this->router = $router;
        $this->auth = $auth;
        $this->userTable = $userTable;
    }

    public function __invoke(Request $request) {
        $langcode = $request->getAttribute("langcode");
        $user = $this->auth->getUser();
        if ($user != null) {
            $this->userTable->update($user->getId(), [
                "langcode" => $langcode
            ]);
        } else {
            setcookie('langcode', $langcode, time() + 3600 * 24 * 3, '/', 'redbricks.games', true, true);
        }
        //echo '<pre>';        var_dump($this->renderer); echo '</pre>'; die();

        return $this->redirect('blog.index');
    }

}
