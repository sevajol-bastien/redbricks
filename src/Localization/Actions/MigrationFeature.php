<?php

namespace App\Localization\Actions;

use Framework\Actions\RouterAwareAction;
use App\Blog\Table\FeatureTable;
use App\Blog\Table\FeatureContentTable;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Psr\Http\Message\ServerRequestInterface as Request;

class MigrationFeature {

    protected $featureTable;
    protected $featureContentTable;

    use RouterAwareAction;

    public function __construct(
            RendererInterface $renderer, Router $router, FeatureTable $featureTable, FeatureContentTable $featureContentTable
    ) {
        $this->renderer = $renderer;
        $this->router = $router;
        $this->featureTable = $featureTable;
        $this->featureContentTable = $featureContentTable;
    }

    public function __invoke(Request $request) {
        $features = $this->featureTable->findAll();
        foreach ($features as $feature) {
            $this->featureContentTable->insert([
                "feature_id" => $feature->getId(),
                "name" => $feature->getName(),
                "content" => $feature->getContent(),
                "langcode" => 'en'
            ]);
        }
        //echo '<pre>';        var_dump($this->renderer); echo '</pre>'; die();

        return $this->redirect('blog.index');
    }

}
