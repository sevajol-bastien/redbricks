<?php

namespace App\News\Actions;

use Framework\Actions\CrudAction;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Framework\Session\FlashService;
use App\News\Table\NewsTable;
use App\Comments\Table\CommentsTable;
use App\Notification\SendNotification;
use App\News\Entity\News;
use Psr\Http\Message\ServerRequestInterface;
use Framework\Session\SessionInterface;
use Framework\Database\Hydrator;
use App\Blog\Table\GameTable;
use App\Blog\Table\ContributorTable;
use Framework\Auth;

class NewsCrudAction extends CrudAction {

    /**
     * @var SessionInterface
     */
    protected $session;

    /**
     *
     * @var SendNotification
     */
    protected $sendNotification;

    /**
     *
     * @var CommentsTable
     */
    protected $commentsTable;

    /**
     *
     * @var GameTable
     */
    protected $gameTable;

    /**
     *
     * @var ContributorTablr
     */
    protected $contributorTable;
    protected $acceptedParams = ["title", "project_id", "content"];

    /**
     * 
     * @param RendererInterface $renderer
     * @param Router $router
     * @param NewsTable $table
     * @param FlashService $flash
     */
    public function __construct(
            RendererInterface $renderer, Router $router, NewsTable $table, FlashService $flash, SessionInterface $session, CommentsTable $commentsTable, SendNotification $sendNotification, GameTable $gameTable, ContributorTable $contributorTable) {
        parent::__construct($renderer, $router, $table, $flash);

        $this->session = $session;
        $this->commentsTable = $commentsTable;
        $this->sendNotification = $sendNotification;
        $this->gameTable = $gameTable;
        $this->contributorTable = $contributorTable;
        $this->messages = [
            "create" => _("A News has been created."),
            "edit" => _("The News has been modified.")
        ];
    }

    public function create(ServerRequestInterface $request) {
        $item = $this->getNewEntity();
        $item->setProjectId($request->getAttribute('projectId'));
        $game = $this->gameTable->find($request->getAttribute('projectId'));
        $this->validateAccessRights($game->getUserId(), $game->getId());
        if ($request->getMethod() === "POST") {
            $validator = $this->getValidator($request);
            if ($validator->isValid()) {
                $news = $this->prePersist($request, $item);
                $this->table->insert($news);
                $news['id'] = $this->table->getPdo()->lastInsertId();
                $this->postPersist($request, $item);
                $this->flash->success($this->messages["create"]);
                $this->sendNotification->send('news', [
                    "gameId" => $item->getProjectId(),
                    "news" => $news,
                    "list" => true
                ]);

                return $this->redirect("news", [
                            "id" => $item->getProjectId()
                ]);
            }
            Hydrator::hydrate($request->getParsedBody(), $item);
            $errors = $validator->getErrors();
        }

        return $this->renderer->render(
                        "@news/create", $this->formParams(compact("item", "errors")));
    }

    public function edit(ServerRequestInterface $request) {
        $item = $this->table->find($request->getAttribute("id"));
        $this->validateAccessRights($item->getUserId(), $item->getProjectId());
        if ($request->getMethod() === "POST") {
            $params = $request->getParsedBody();
            $validator = $this->getValidator($request);
            if ($validator->isValid()) {
                $fields = $this->prePersist($request, $item);
                array_pop($fields);
                $this->table->update($item->getId(), $fields);
                $this->flash->success($this->messages["edit"]);
                $this->postPersist($request, $item);

                return $this->redirect("news", [
                            "id" => $item->getProjectId()
                ]);
            }
            $errors = $validator->getErrors();
            Hydrator::hydrate($request->getParsedBody(), $item);
        }

        return $this->renderer->render(
                        "@news/edit", $this->formParams(compact("item", "errors")));
    }

    public function delete(ServerRequestInterface $request) {
        $newsId = $request->getAttribute('id');
        $projectId = $request->getAttribute('projectId');
        $game = $this->gameTable->find($projectId);
        $this->validateAccessRights($game->getUserId(), $game->getId());
        $comments = $this->commentsTable->findBy("news_id", $newsId)->fetchAll();
        $this->postPersist($request, $this->table->find($newsId));
        foreach ($comments->records as $comment) {
            $this->commentsTable->delete($comment["id"]);
        }
        $this->table->delete($newsId);
        $this->flash->success(_("Successful deletion"));
        return $this->redirect("news", ['id' => $projectId]);
    }

    protected function getNewEntity() {
        $news = new News();
        $news->setCreatedAt(new \DateTime());
        return $news;
    }

    protected function getValidator(ServerRequestInterface $request) {
        $validator = parent::getValidator($request)
                ->required("title", "content")
                ->length("title", 2, 200)
                ->length("content", 10);

        return $validator;
    }

    protected function prePersist(ServerRequestInterface $request, $news): array {
        $params = $request->getParsedBody();

        if ($this->session->get('auth.role')) {
            $userId = $this->session->get('auth.user');
        } else {
            $userId = null;
        }

        if (!empty($params["projectId"])) {
            $params["project_id"] = $params["projectId"];
        }

        $params = array_filter($params, function ($key) {
            return in_array($key, $this->acceptedParams);
        }, ARRAY_FILTER_USE_KEY);
        return array_merge($params, ["updated_at" => date("Y-m-d H:i:s"),
            "created_at" => $news->getCreatedAt()->format("Y-m-d H:i:s"),
            "user_id" => $userId]);
    }

    protected function postPersist(ServerRequestInterface $request, $item): void {
        $game = $this->gameTable->find($item->getProjectId());
        $this->gameTable->update($game->getId(), [
            "updated_at" => date("Y-m-d H:i:s")
        ]);
    }

    protected function validateAccessRights(int $itemUserId, int $gameId) {
        $role = $this->session->get('auth.role');
        $userId = $this->session->get('auth.user');
        if ($role == null || ($role == 'user' && $userId != $itemUserId && !$this->contributorTable->isContributor($gameId, $userId))) {
            throw new Auth\ForbiddenException();
        }
    }

}
