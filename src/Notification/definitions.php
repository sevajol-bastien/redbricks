<?php
return [
    'notification.from' => \DI\get('mail.from'),
    \App\Notification\SendNotification::class => \DI\object()->constructorParameter('from', \DI\get('notification.from'))
];
