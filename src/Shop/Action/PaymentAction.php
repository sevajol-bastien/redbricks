<?php

namespace App\Shop\Action;

use Framework\Renderer\RendererInterface;
use App\Blog\Table\GameTable;
use App\Auth\UserTable;
use App\Shop\Table\RewardTable;
use Psr\Http\Message\ServerRequestInterface;
use Framework\Router;
use Framework\Api\Stripe;
use Framework\Auth;

class PaymentAction {

    /**
     * @var RendererInterface
     */
    protected $renderer;

    /**
     *
     * @var GameTable
     */
    protected $gameTable;

    /**
     *
     * @var UserTable
     */
    protected $userTable;

    /**
     *
     * @var RewardTable
     */
    protected $rewardTable;

    /**
     *
     * @var Stripe
     */
    protected $stripe;

    /**
     * @var string
     */
    private $stripeKey;

    /**
     *
     * @var Router
     */
    protected $router;

    /**
     *
     * @var Auth
     */
    protected $auth;

    public function __construct(
            RendererInterface $renderer, GameTable $gameTable, UserTable $userTable, RewardTable $rewardTable, Stripe $stripe, string $stripeKey, Router $router, Auth $auth
    ) {
        $this->renderer = $renderer;
        $this->gameTable = $gameTable;
        $this->userTable = $userTable;
        $this->rewardTable = $rewardTable;
        $this->stripe = $stripe;
        $this->stripeKey = $stripeKey;
        $this->router = $router;
        $this->auth = $auth;
    }

    public function __invoke(ServerRequestInterface $request) {
        $gameId = $request->getAttribute("gameId");
        $rewardId = $request->getAttribute("rewardId");
        $game = $this->gameTable->find($gameId);
        $user = $this->userTable->find($game->getUserId());
        $params = $request->getParsedBody();
        $amount = $params['amount'];
        $reward = $this->rewardTable->find($rewardId);

        $stripeKey = $this->stripeKey;
        $stripeAccount = $user->getStripeUserId();
        $url = 'http://localhost:8000' . $this->router->generateUri('blog.show', ['slug' => $game->getSlug(), 'id' => $gameId]);
        $timestamp = strtotime("today first day of next month");
        $metadata = [
            "user_id" => $this->auth->getUser()->getId(),
            "game_id" => $game->getId(),
            "reward_id" => $rewardId
        ];
        $planId = $this->getPlanId($rewardId, $amount, $stripeAccount);

        $session = $this->stripe->createCheckoutSession([
            'billing_address_collection' => 'required',
            'payment_method_types' => ['card'],
            'client_reference_id' => $stripeAccount,
            'subscription_data' => [
                'items' => [[
                'plan' => $planId,
                    ]],
                'application_fee_percent' => $game->getFee(),
                'trial_end' => $timestamp,
                'metadata' => $metadata,
            ],
            'success_url' => $url,
            'cancel_url' => $url,
                ], ['stripe_account' => $stripeAccount]);

        return $this->renderer->render('@shop/payment', compact('session', 'stripeKey', 'game', 'stripeAccount', 'reward', 'amount'));
    }

    protected function getPlanId(int $rewardId, int $amount, string $stripeAccount): string {
        $amount *= 100;
        $plans = $this->stripe->allPlan([], ["stripe_account" => $stripeAccount])->data;
        $plan = null;
        $productId = null;
        if ($rewardId != 0) {
            $reward = $this->rewardTable->find($rewardId);
            $productId = $reward->getProductId();
        } else {
            $products = $this->stripe->allProduct([], ["stripe_account" => $stripeAccount])->data;
            foreach ($products as $product) {
                if ($product->name == "Subscription without reward") {
                    $productId = $product->id;
                }
            }
            if (is_null(productId)) {
                $product = $this->stripe->createProduct([
                    "name" => "Subscription without reward",
                    "type" => "service"
                ], ["stripe_account" => $stripeAccount]);
                $productId = $product->id;
            }
        }
        foreach ($plans as $planResearch) {
            if ($planResearch->amount == $amount && $planResearch->product == $productId) {
                $plan = $planResearch;
            }
        }
        if ($plan == null) {
            $plan = $this->stripe->createPlan([
                "amount" => $amount,
                "interval" => "month",
                "product" => $productId,
                "currency" => "eur"
                    ], array("stripe_account" => $stripeAccount));
        }
        return $plan->id;
    }

}
