<?php

namespace App\Shop\Action;

use App\Shop\PurchaseProduct;
use Framework\Actions\RouterAwareAction;
use Framework\Auth;
use Framework\Router;
use Framework\Session\FlashService;
use Psr\Http\Message\ServerRequestInterface;
use App\Blog\Table\GameTable;
use App\Blog\Table\FeatureTable;
use App\Shop\Table\PendingPurchaseTable;
use App\Notification\SendNotification;

class PurchaseProcessAction {

    /**
     * @var Auth
     */
    private $auth;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var FlashService
     */
    private $flashService;

    /**
     *
     * @var GameTable
     */
    protected $gameTable;

    /**
     *
     * @var FeatureTable
     */
    protected $featureTable;

    /**
     *
     * @var PendingPurchaseTable
     */
    protected $pendingPurchaseTable;

    /**
     *
     * @var SendNotification
     */
    protected $sendNotification;

    use RouterAwareAction;

    public function __construct(
            PurchaseProduct $purchaseProduct, Auth $auth, Router $router, FlashService $flashService, GameTable $gameTable, FeatureTable $featureTable, PendingPurchaseTable $pendingPurchaseTable, SendNotification $sendNotification
    ) {
        $this->purchaseProduct = $purchaseProduct;
        $this->auth = $auth;
        $this->router = $router;
        $this->flashService = $flashService;
        $this->gameTable = $gameTable;
        $this->featureTable = $featureTable;
        $this->pendingPurchaseTable = $pendingPurchaseTable;
        $this->sendNotification = $sendNotification;
    }

    public function __invoke(ServerRequestInterface $request) {
        $params = $request->getParsedBody();
        $params['gameId'] = $request->getAttribute('gameId');
        $params['featureId'] = $request->getAttribute('featureId');
        $stripeToken = $params['stripeToken'];
        if (/*$params['featureId'] != 0*/ !isset($params['subscription']) || $params['subscription'] != true) {
            $fee = 0;
            if ($params['wantFee'] == 'yes') {
                $fee = $params['feeUser'];
            }
            //echo '<pre>'; var_dump($fee, $request, $stripeToken); echo '</pre>'; die();
            $this->purchaseProduct->process($params['totAmount'], $this->auth->getUser(), $stripeToken, $params['gameId'], $params['featureId'], $fee);

            $this->sendNotification->send('tip', [
                "gameId" => $params['gameId'],
            ]);

            $this->flashService->success(_('Thank you for your contribution'));
            if (!empty($params['featureId']) && $params['featureId'] != 0) {
                $feature = $this->featureTable->find($params['featureId']);

                $pendingPurchases = $this->pendingPurchaseTable->getMoneyRaised("p.feature_id", $params['featureId']);
                if ($pendingPurchases >= $feature->getGoal()) {
                    $this->sendNotification->send('collect', [
                        "gameId" => $params['gameId'],
                        "featureId" => $params['featureId'],
                    ]);
                }

                return $this->redirect('blog.feature.showOneFeature', ['slug' => $feature->getSlug(), 'id' => $feature->getId()]);
            }
        } else {
            //echo '<pre>'; var_dump($params, $stripeToken); echo '</pre>'; die();
            if($params['sourceAmount'] == "selected") {
                $params['totAmount'] = $params['quantity'] * 100;
            } else {
                $params['totAmount'] = $params['amountFix'] * 100;
            }
            $this->purchaseProduct->subscription($params['totAmount'], $this->auth->getUser(), $stripeToken, $params['gameId']);
            $this->flashService->success(_('Thank you for your subscription'));
        }
        
        $game = $this->gameTable->find($params['gameId']);
        return $this->redirect('blog.show', ['slug' => $game->getSlug(), 'id' => $game->getId()]);
    }

}
