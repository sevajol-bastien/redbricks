<?php

namespace App\Shop\Action;

use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Framework\Session\SessionInterface;
use Framework\Auth;
use App\Blog\Table\GameTable;
use App\Blog\Table\ContributorTable;
use App\Shop\Table\RewardTable;

class RewardIndexAction {

    /**
     *
     * @var RendererInterface
     */
    protected $renderer;

    /**
     *
     * @var GameTable
     */
    protected $gameTable;
    
    /**
     *
     * @var RewardTable
     */
    protected $rewardTable;

    /**
     *
     * @var ContributorTable
     */
    protected $contributorTable;

    /**
     *
     * @var SessionInterface
     */
    protected $session;

    public function __construct(
            RendererInterface $renderer, GameTable $gameTable, ContributorTable $contributorTable, RewardTable $rewardTable, SessionInterface $session
    ) {
        $this->renderer = $renderer;
        $this->gameTable = $gameTable;
        $this->contributorTable = $contributorTable;
        $this->rewardTable = $rewardTable;
        $this->session = $session;
    }

    public function __invoke(Request $request) {
        $id = $request->getAttribute("id");
        $game = $this->gameTable->findShow($id);
        $this->validateAccessRights($game->getUserId(), $game->getId());
        $rewards = $this->rewardTable->findAllBy("project_id", $id);

        return $this->renderer->render("@shop/rewards/index", compact("game", "rewards"));
    }

    protected function validateAccessRights(int $itemUserId, int $gameId) {
        $role = $this->session->get('auth.role');
        $userId = $this->session->get('auth.user');
        if ($role == null || ($role == 'user' && $userId != $itemUserId && !$this->contributorTable->isContributor($gameId, $userId))) {
            throw new Auth\ForbiddenException();
        }
    }

}
