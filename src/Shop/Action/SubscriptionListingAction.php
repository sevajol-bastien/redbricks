<?php

namespace App\Shop\Action;

use Framework\Renderer\RendererInterface;
use App\Shop\Table\SubscriptionTable;
use Framework\Auth;
use Framework\Api\Stripe;

class SubscriptionListingAction {

    /**
     *
     * @var RendererInterface
     */
    protected $renderer;

    /**
     *
     * @var SubscriptionTable
     */
    protected $subscriptionTable;

    /**
     *
     * @var Auth
     */
    protected $auth;

    /**
     *
     * @var Stripe
     */
    protected $stripe;

    public function __construct(
            RendererInterface $renderer, SubscriptionTable $subscriptionTable, Auth $auth, Stripe $stripe
    ) {
        $this->renderer = $renderer;
        $this->subscriptionTable = $subscriptionTable;
        $this->auth = $auth;
        $this->stripe = $stripe;
    }

    public function __invoke() {
        $user_id = $this->auth->getUser()->getId();
        $subs= $this->subscriptionTable->findAllByUser($user_id);
        foreach ($subs as $subscription) {
            $subscriptions[$subscription->getId()] = $sub = $this->stripe->getSubscription($subscription->getSubscriptionId(), array("stripe_account" => $subscription->getStripeAccount()));
            $subscriptions[$subscription->getId()]["gameName"] = $subscription->gameName;
        $product = $this->stripe->getProduct($sub['plan']['product'], array("stripe_account" => $subscription->getStripeAccount()));
            $subscriptions[$subscription->getId()]['product'] = $product['name'];
        }
        
        return $this->renderer->render('@shop/manageSubscription', compact('subscriptions'));
    }

}
