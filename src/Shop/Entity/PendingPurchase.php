<?php

namespace App\Shop\Entity;

class PendingPurchase {

    /**
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var int
     */
    protected $userId;

    /**
     *
     * @var int
     */
    protected $featureId;

    /**
     *
     * @var int
     */
    protected $gameId;

    /**
     *
     * @var int
     */
    protected $cardId;

    /**
     *
     * @var int
     */
    protected $fee;

    /**
     *
     * @var int
     */
    protected $price;

    /**
     *
     * @var \DateTine
     */
    protected $createdAt;

    /**
     *
     * @var string
     */
    protected $customerId;

    /**
     *
     * @var string
     */
    protected $stripeUserId;

    /**
     * 
     * @return int
     */
    function getId(): int {
        return $this->id;
    }

    /**
     * 
     * @return int
     */
    function getUserId(): int {
        return $this->userId;
    }

    /**
     * 
     * @return int
     */
    function getFeatureId(): int {
        return $this->featureId;
    }

    /**
     * 
     * @return int
     */
    function getGameId(): int {
        return $this->gameId;
    }

    /**
     * 
     * @return string
     */
    function getCardId(): string {
        return $this->cardId;
    }

    /**
     * 
     * @return int
     */
    function getFee(): int {
        return $this->fee;
    }

    /**
     * 
     * @return int
     */
    function getPrice(): int {
        return $this->price;
    }

    /**
     * 
     * @return \DateTime
     */
    function getCreatedAt(): \DateTime {
        return $this->createdAt;
    }

    /**
     * 
     * @param int $id
     */
    function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @param int $userId
     */
    function setUserId(int $userId) {
        $this->userId = $userId;
    }

    /**
     * 
     * @param int $featureId
     */
    function setFeatureId(int $featureId) {
        $this->featureId = $featureId;
    }

    /**
     * 
     * @param int $gameId
     */
    function setGameId(int $gameId) {
        $this->gameId = $gameId;
    }

    /**
     * 
     * @param string $cardId
     */
    function setCardId(string $cardId) {
        $this->cardId = $cardId;
    }

    /**
     * 
     * @param int $fee
     */
    function setFee(int $fee) {
        $this->fee = $fee;
    }

    /**
     * 
     * @param int $price
     */
    function setPrice(int $price) {
        $this->price = $price;
    }

    /**
     * @param \DateTime|string|null $createdAt
     */
    function setCreatedAt($createdAt) {
        if (is_string($createdAt)) {
            $this->createdAt = new \DateTime($createdAt);
        } else {
            $this->createdAt = $createdAt;
        }
    }

    /**
     * 
     * @return string
     */
    function getCustomerId(): string {
        return $this->customerId;
    }

    /**
     * 
     * @param string $customerId
     */
    function setCustomerId(string $customerId) {
        $this->customerId = $customerId;
    }

    /**
     * 
     * @return string
     */
    function getStripeUserId(): string {
        return $this->stripeUserId;
    }

    /**
     * 
     * @param string $stripeUserId
     */
    function setStripeUserId(string $stripeUserId) {
        $this->stripeUserId = $stripeUserId;
    }

}
