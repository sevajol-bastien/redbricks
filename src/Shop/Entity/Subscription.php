<?php

namespace App\Shop\Entity;

class Subscription {

    /**
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var int
     */
    protected $userId;

    /**
     *
     * @var int
     */
    protected $gameId;

    /**
     *
     * @var int
     */
    protected $rewardId;

    /**
     *
     * @var string
     */
    protected $subscriptionId;

    /**
     *
     * @var string
     */
    protected $planId;

    /**
     *
     * @var string
     */
    protected $stripeAccount;

    /**
     *
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * 
     * @return int
     */
    function getId(): int {
        return $this->id;
    }

    /**
     * 
     * @return int
     */
    function getUserId(): int {
        return $this->userId;
    }

    /**
     * 
     * @return int
     */
    function getGameId(): int {
        return $this->gameId;
    }

    /**
     * 
     * @return int
     */
    function getRewardId(): int {
        return $this->rewardId;
    }

    /**
     * 
     * @return string
     */
    function getSubscriptionId(): string {
        return $this->subscriptionId;
    }

    /**
     * 
     * @return string
     */
    function getPlanId(): string {
        return $this->planId;
    }

    /**
     * 
     * @return string
     */
    function getStripeAccount(): string {
        return $this->stripeAccount;
    }

    /**
     * 
     * @return \DateTime
     */
    function getCreatedAt(): \DateTime {
        return $this->createdAt;
    }

    /**
     * 
     * @param int $id
     */
    function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @param int $userId
     */
    function setUserId(int $userId) {
        $this->userId = $userId;
    }

    /**
     * 
     * @param int $gameId
     */
    function setGameId(int $gameId) {
        $this->gameId = $gameId;
    }

    /**
     * 
     * @param int $rewardId
     */
    function setRewardId(int $rewardId) {
        $this->rewardId = $rewardId;
    }

    /**
     * 
     * @param string $subscriptionId
     */
    function setSubscriptionId(string $subscriptionId) {
        $this->subscriptionId = $subscriptionId;
    }

    /**
     * 
     * @param string $planId
     */
    function setPlanId(string $planId) {
        $this->planId = $planId;
    }

    /**
     * 
     * @param string $stripeAccount
     */
    function setStripeAccount(string $stripeAccount) {
        $this->stripeAccount = $stripeAccount;
    }

    /**
     * 
     * @param \DateTime | string $createdAt
     */
    function setCreatedAt($createdAt) {
        if (is_string($createdAt)) {
            $this->createdAt = new \DateTime($createdAt);
        } else {
            $this->createdAt = $createdAt;
        }
    }

}
