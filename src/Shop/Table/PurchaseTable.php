<?php

namespace App\Shop\Table;

use App\Auth\User;
use App\Shop\Entity\Purchase;
use Framework\Database\QueryResult;
use Framework\Database\Table;
use App\Auth\UserTable;
use Framework\Database\Query;

class PurchaseTable extends Table {

    protected $entity = Purchase::class;
    protected $table = "purchases";

    /**
     * 
     * @param User $user
     * @return QueryResult
     */
    public function findForUser(User $user): QueryResult {
        return $this->makeQuery()
                        ->select('p.*')
                        ->where('p.user_id = :user')
                        ->params(['user' => $user->getId()])
                        ->order("p.created_at DESC")
                        ->fetchAll();
    }

    /**
     * 
     * @param int $featureId
     * @return QueryResult
     */
    public function findForFeature(int $featureId): QueryResult {
        $user = new UserTable($this->pdo);
        return $this->makeQuery()
                        ->select('p.*, u.displayname')
                        ->join($user->getTable() . " as u", "u.id = p.user_id")
                        ->where('p.feature_id = :featureId')
                        ->params(['featureId' => $featureId])
                        ->fetchAll();
    }

    /**
     * 
     * @param int $gameId
     * @return QueryResult
     */
    public function findForProject(int $gameId): QueryResult {
        return $this->makeQuery()
                        ->select('p.*')
                        ->where('p.game_id = :gameId')
                        ->params(['gameId' => $gameId])
                        ->fetchAll();
    }

    /**
     * 
     * @return mixed
     */
    public function getMonthRevenue() {
        return $this->makeQuery()
                        ->select('SUM(price)/100')
                        ->where("p.created_at BETWEEN DATE_SUB(NOW(), INTERVAL 1 MONTH) AND NOW()")
                        ->fetchColumnNum();
    }

    /**
     * 
     * @param string $field
     * @param int $id
     * @return mixed
     */
    public function getMoneyRaised(string $field, int $id) {
        return $this->makeQuery()
                        ->select('SUM(price)/100')
                        ->where("$field = $id")
                        ->fetchColumnNum();
    }

    /**
     * 
     * @param string $field
     * @param int $id
     * @return mixed
     */
    public function countParticipants(string $field, int $id) {
        return $this->makeQuery()
                        ->select("count(DISTINCT user_id)")
                        ->where("$field = $id")
                        ->fetchColumnNum();
    }

    /**
     * 
     * @return Query
     */
    public function getAllMoneyRaised(): Query {
        return $this->makeQuery()
                        ->select('SUM(price)/100 as price, p.game_id')
                        ->groupBy('game_id');
    }

    /**
     * 
     * @param type $game_id
     * @return Query
     */
    public function getAllMoneyRaisedForProject($game_id): Query {
        return $this->makeQuery()
                        ->select('SUM(price)/100 as price, feature_id')
                        ->where("game_id = $game_id")
                        ->groupBy('feature_id');
    }

}
