<?php

namespace App\Shop\Table;

use Framework\Database\Table;
use App\Shop\Entity\Reward;

class RewardTable extends Table {

    protected $entity = Reward::class;
    protected $table = "rewards";

}
