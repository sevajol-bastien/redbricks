<?php
namespace App\Start;

use Framework\Auth;
use Framework\Session\SessionInterface;
//use Framework\Renderer\TwigRenderer;
use Framework\Renderer\RendererInterface;
use Framework\Response\RedirectResponse;
use Framework\Session\FlashService;
use Framework\Validator;
use Psr\Http\Message\ServerRequestInterface;

class StartAction
{
	//protected $path= "/start";
	protected $routePrefix= "blog.user";
	protected $routeReturn= "start";
    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var FlashService
     */
    private $flashService;
    /**
     * @var SessionInterface
     */
    private $session;
	
    public function __construct(
        RendererInterface $renderer,
        FlashService $flashService,
		Auth $auth,
        SessionInterface $session
    ) {
    
        $this->renderer = $renderer;
        $this->flashService = $flashService;
		$this->auth = $auth;
		$this->session = $session;
    }

    /**
     * @param ServerRequestInterface $request
     * @return RedirectResponse|string
     */
    public function __invoke(ServerRequestInterface $request)
    {
        $this->renderer->addGlobal("routePrefix", $this->routePrefix);
		$this->session->set("routeReturn", "start");
		if ($request->getMethod() === 'GET') {
			if ($this->auth->getUser()) {
				$userId= $this->auth->getUser()->getId();
			} else {
				$userId= "";
			}
			return $this->renderer->render('@start/start', compact('userId'));
        }
        $params = $request->getParsedBody();
    }
}
