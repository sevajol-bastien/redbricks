<?php

namespace App\Suggestion\Actions;

use Framework\Renderer\RendererInterface;
use Framework\Response\RedirectResponse;
use Psr\Http\Message\ServerRequestInterface;
use App\Suggestion\Table\SuggestionTable;
use App\Suggestion\Table\VotesSuggestionTable;
use Framework\Auth;
use App\Auth\User;
use Framework\Validator;
use Framework\Session\FlashService;

class SuggestionAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     *
     * @var SuggestionTable
     */
    protected $suggestionTable;

    /**
     *
     * @var VotesSuggestionTable
     */
    protected $votesSuggestionTable;

    /**
     *
     * @var Auth
     */
    protected $auth;

    /**
     *
     * @var FlashService
     */
    protected $flashService;

    public function __construct(
            RendererInterface $renderer, SuggestionTable $suggestionTable, VotesSuggestionTable $votesSuggestionTable, Auth $auth, FlashService $flashService
    ) {
        $this->renderer = $renderer;
        $this->suggestionTable = $suggestionTable;
        $this->votesSuggestionTable = $votesSuggestionTable;
        $this->auth = $auth;
        $this->flashService = $flashService;
    }

    /**
     * @param ServerRequestInterface $request
     * @return RedirectResponse|string
     */
    public function __invoke(ServerRequestInterface $request) {
        $user = $this->auth->getUser();

        if ($request->getMethod() == "POST") {
            $new = $this->post($request->getParsedBody(), $user);
        }

        $array = $this->order($request);
        $order = $array['order'];
        $category = $array['category'];
        $sort = $array['sort'];

        $query = $request->getQueryParams();
        if ($user != null) {
            if ($request->getMethod() == "GET" && !empty($query['search'])) {
                $search = $query['search'];
                $suggestions = $this->suggestionTable->findWithSearchForUser($search, $user->getId(), $order);
            } else {
                $suggestions = $this->suggestionTable->findAllForUser($user->getId(), $order);
            }
        } else {
            if ($request->getMethod() == "GET" && !empty($query['search'])) {
                $search = $query['search'];
                $suggestions = $this->suggestionTable->findWithSearch($search, $order);
            } else {
                $suggestions = $this->suggestionTable->findAllWithOrder($order);
            }
        }
        return $this->renderer->render('@suggestion/suggest', compact("suggestions", "new", "category", "sort", "search"));
    }

    protected function post(array $params, User $user): ?array {
        if (isset($params['add'])) {
            $this->votesSuggestionTable->insert([
                "suggestion_id" => $params['suggestionId'],
                "user_id" => $user->getId(),
                "created_at" => date("Y-m-d H:i:s")
            ]);
            $this->flashService->success(_("Your vote has been recorded"));
        } else if (isset($params['remove'])) {
            $this->votesSuggestionTable->deleteBySuggest($params['suggestionId'], $user->getId());
            $this->flashService->error(_("Your vote has been removed"));
        } else if (isset($params['new'])) {
            $errors = $this->newGame($params, $user);
            if ($errors) {
                $this->flashService->error(current($errors));
                return [
                    "studio" => $params['studio'],
                    "game" => $params['game']
                ];
            } else {
                $this->flashService->success(_("The game has just been added"));
            }
        }
        return null;
    }

    protected function order(ServerRequestInterface $request): array {
        $order = "s.studio, s.game";
        if ($request->getAttribute('category') && $request->getAttribute('sort')) {
            $category = $request->getAttribute('category');
            $sort = $request->getAttribute('sort');
            $order = $category;
            if ($sort == "up") {
                $order .= " DESC";
            }
            if ($category == "votes") {
                $order .= ", s.studio, s.game";
            }
            if ($category == "studio") {
                $order .= ", s.game";
            }
        }

        return [
            "order" => $order,
            "category" => $category ?? null,
            "sort" => $sort ?? null
        ];
    }

    protected function newGame(array $params, User $user) {

        $validator = $this->getValidator($params);
        if ($validator->isValid()) {
            $this->suggestionTable->insert([
                "game" => $params['game'],
                "studio" => $params['studio'],
                "created_at" => date("Y-m-d H:i:s")
            ]);
            $suggestionId = $this->suggestionTable->getPdo()->lastInsertId();
            $this->votesSuggestionTable->insert([
                "suggestion_id" => $suggestionId,
                "user_id" => $user->getId(),
                "created_at" => date("Y-m-d H:i:s")
            ]);
        }
        return $validator->getErrors();
    }

    protected function getValidator(array $params) {
        $validator = new Validator($params);
        return $validator->required("studio", "game")
                        ->length("studio", 3, 100)
                        ->length("game", 3, 100)
                        ->unique2("game", "studio", $this->suggestionTable);
    }

}
