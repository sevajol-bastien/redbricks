<?php

namespace App\Support\Action;

use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Support\Table\ArticleTable;
use App\Support\Table\SectionTable;

class SectionAction {

    /**
     *
     * @var RendererInterface
     */
    protected $renderer;

    /**
     *
     * @var SectionTable
     */
    protected $sectionTable;

    /**
     *
     * @var ArticleTable
     */
    protected $articleTable;

    public function __construct(
    RendererInterface $renderer, SectionTable $sectionTable, ArticleTable $articleTable
    ) {
        $this->renderer = $renderer;
        $this->sectionTable = $sectionTable;
        $this->articleTable = $articleTable;
    }

    public function __invoke(Request $request) {

        $id = $request->getAttribute("id");
        $section = $this->sectionTable->find($id);
        $articles = $this->articleTable->findForSection($id);
        return $this->renderer->render("@support/section", compact('section', 'articles'));
    }

}
