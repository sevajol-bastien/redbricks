<?php

namespace App\Support\Action;

use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Support\Table\ArticleTable;
use App\Support\Table\SectionTable;

class SupportAction {

    /**
     *
     * @var RendererInterface
     */
    protected $renderer;
    
    /**
     *
     * @var SectionTable
     */
    protected $sectionTable;
    
    /**
     *
     * @var ArticleTable
     */
    protected $articleTable;

    public function __construct(
    RendererInterface $renderer, SectionTable $sectionTable, ArticleTable $articleTable
    ) {
        $this->renderer = $renderer;
        $this->sectionTable = $sectionTable;
        $this->articleTable = $articleTable;
    }

    public function __invoke(Request $request) {

        $sections = $this->sectionTable->findAll();
        $articles = $this->articleTable->findAll();
        return $this->renderer->render("@support/index", compact('sections', 'articles'));
    }

}
