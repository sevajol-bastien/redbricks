<?php

namespace App\Support\Entity;

class Article {

    /**
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var int
     */
    protected $sectionId;

    /**
     *
     * @var string
     */
    protected $nameFr;

    /**
     *
     * @var string
     */
    protected $slugFr;

    /**
     *
     * @var string
     */
    protected $contentFr;

    /**
     *
     * @var string
     */
    protected $sectionNameFr;

    /**
     *
     * @var string
     */
    protected $sectionSlugFr;

    /**
     * 
     * @return int
     */
    function getId(): int {
        return $this->id;
    }

    /**
     * 
     * @return int
     */
    function getSectionId(): int {
        return $this->sectionId;
    }

    /**
     * 
     * @return string
     */
    function getNameFr(): string {
        return $this->nameFr;
    }

    /**
     * 
     * @return string
     */
    function getSlugFr(): string {
        return $this->slugFr;
    }

    /**
     * 
     * @return string
     */
    function getContentFr(): string {
        return $this->contentFr;
    }

    /**
     * 
     * @return string
     */
    function getSectionNameFr(): string {
        return $this->sectionNameFr;
    }

    /**
     * 
     * @return string
     */
    function getSectionSlugFr(): string {
        return $this->sectionSlugFr;
    }

    /**
     * 
     * @param int $id
     */
    function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @param int $sectionId
     */
    function setSectionId(int $sectionId) {
        $this->sectionId = $sectionId;
    }

    /**
     * 
     * @param string $nameFr
     */
    function setNameFr(string $nameFr) {
        $this->nameFr = $nameFr;
    }

    /**
     * 
     * @param string $slugFr
     */
    function setSlugFr(string $slugFr) {
        $this->slugFr = $slugFr;
    }

    /**
     * 
     * @param string $contentFr
     */
    function setContentFr(string $contentFr) {
        $this->contentFr = $contentFr;
    }

    /**
     * 
     * @param string $sectionNameFr
     */
    function setSectionNameFr(string $sectionNameFr) {
        $this->sectionNameFr = $sectionNameFr;
    }

    /**
     * 
     * @param string $sectionSlugFr
     */
    function setSectionSlugFr(string $sectionSlugFr) {
        $this->sectionSlugFr = $sectionSlugFr;
    }

}
