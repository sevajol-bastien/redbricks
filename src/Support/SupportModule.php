<?php

namespace App\Support;

use Framework\Module;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use App\Support\Action\SupportAction;
use App\Support\Action\SectionAction;
use App\Support\Action\ArticleAction;

class SupportModule extends Module {

    public function __construct(Router $router, RendererInterface $renderer) {
        $renderer->addPath('support', __DIR__ . "/views");
        $router->get('/support/{loc:[a-z]+}', SupportAction::class, 'support');
        $router->get('/support/{loc:[a-z]+}/sections/{id:[0-9]+}-{slug:[a-z\-0-9]+}', SectionAction::class, 'support.section');
        $router->get('/support/{loc:[a-z]+}/articles/{id:[0-9]+}-{slug:[a-z\-0-9]+}', ArticleAction::class, 'support.article');
    }

}
