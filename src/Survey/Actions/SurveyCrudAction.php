<?php

namespace App\Survey\Actions;

use Framework\Actions\CrudAction;
use Framework\Session\SessionInterface;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Framework\Session\FlashService;
use App\Notification\SendNotification;
use App\Blog\Table\GameTable;
use App\Survey\Table\SurveyTable;
use Psr\Http\Message\ServerRequestInterface;
use App\Survey\Entity\Survey;
use App\Survey\Table\ProposalTable;
use Framework\Database\Hydrator;
use App\Survey\Table\VotesSurveyTable;
use App\Blog\Table\ContributorTable;
use Framework\Auth;

class SurveyCrudAction extends CrudAction {

    /**
     * @var SessionInterface
     */
    protected $session;

    /**
     *
     * @var ProposalTable
     */
    protected $proposalTable;

    /**
     *
     * @var VotesSurveyTable
     */
    protected $votesSurveyTable;

    /**
     *
     * @var ContributorTable
     */
    protected $contributorTable;
    protected $acceptedParams = ["question", "project_id"];

    /**
     * 
     * @param RendererInterface $renderer
     * @param Router $router
     * @param NewsTable $table
     * @param FlashService $flash
     */
    public function __construct(
            RendererInterface $renderer, Router $router, SurveyTable $table, FlashService $flash, SessionInterface $session, SendNotification $sendNotification, GameTable $gameTable, ProposalTable $proposableTable, VotesSurveyTable $votesSurveyTable, ContributorTable $contributorTable) {
        parent::__construct($renderer, $router, $table, $flash);

        $this->session = $session;
        $this->sendNotification = $sendNotification;
        $this->gameTable = $gameTable;
        $this->proposalTable = $proposableTable;
        $this->votesSurveyTable = $votesSurveyTable;
        $this->contributorTable = $contributorTable;
        $this->messages = [
            "create" => _("A survey has been created."),
            "edit" => _("The survey has been modified.")
        ];
    }

    public function create(ServerRequestInterface $request) {
        $item = $this->getNewEntity();
        $item->setProjectId($request->getAttribute('projectId'));
        $game = $this->gameTable->find($item->getProjectId());
        $this->validateAccessRights($game->getUserId(), $game->getId());

        if ($request->getMethod() === "POST") {
            $params = $request->getParsedBody();
            if (isset($params['add'])) {
                return $this->add($params, $item);
            }

            $validator = $this->getValidator($request);
            if ($validator->isValid()) {
                $survey = $this->prePersist($request, $item);
                $this->table->insert($survey);
                $surveyId = $this->table->getPdo()->lastInsertId();
                $i = 1;
                while (isset($params['proposal' . $i]) && !empty($params['proposal' . $i])) {
                    $proposal = $params['proposal' . $i];
                    if (filter_var($proposal, FILTER_VALIDATE_URL)) {
                        $image = 1;
                    }
                    $this->proposalTable->insert([
                        "project_id" => $item->getProjectId(),
                        "survey_id" => $surveyId,
                        "proposal" => $proposal,
                        "image" => $image ?? 0
                    ]);
                    $i++;
                }
                $this->postPersist($request, $item);
                $this->flash->success($this->messages["create"]);

                return $this->redirect("surveys", [
                            "id" => $item->getProjectId()
                ]);
            }
            Hydrator::hydrate($request->getParsedBody(), $item);
            $errors = $validator->getErrors();
            //echo '<pre>';            var_dump($item); echo '</pre>'; die();
        }

        return $this->renderer->render(
                        "@survey/create", $this->formParams(compact("item", "errors")));
    }

    protected function getNewEntity() {
        $survey = new Survey();
        $survey->setCreatedAt(new \DateTime());
        return $survey;
    }

    protected function getValidator(ServerRequestInterface $request) {
        $params = $request->getParsedBody();
        $validator = parent::getValidator($request)
                ->required("question", "proposal1", "proposal2")
                ->length("question", 10, 255)
                ->length('proposal1', 1, 100)
                ->length('proposal2', 1, 100);
        $i = 3;
        while (isset($params['proposal' . $i]) && !empty($params['proposal' . $i])) {
            $validator->length('proposal' . $i, 1, 100);
            $i++;
        }

        return $validator;
    }

    protected function prePersist(ServerRequestInterface $request, $survey): array {
        $params = $request->getParsedBody();

        if ($this->session->get('auth.role')) {
            $userId = $this->session->get('auth.user');
        } else {
            $userId = null;
        }

        if (!empty($params["projectId"])) {
            $params["project_id"] = $params["projectId"];
        }

        $params = array_filter($params, function ($key) {
            return in_array($key, $this->acceptedParams);
        }, ARRAY_FILTER_USE_KEY);
        return array_merge($params, [
            "created_at" => $survey->getCreatedAt()->format("Y-m-d H:i:s"),
            "user_id" => $userId]);
    }

    public function add(array $params, Survey $item) {

        $nbProposal = round($params['nbProposal'] + $params['nbFields']);
        if ($nbProposal < 4) {
            $nbProposal = 4;
        }
        Hydrator::hydrate($params, $item);
        return $this->renderer->render(
                        "@survey/create", $this->formParams(compact("item", "nbProposal")));
    }

    public function delete(ServerRequestInterface $request) {
        $surveyId = $request->getAttribute('id');
        $projectId = $request->getAttribute('projectId');
        $survey = $this->table->find($surveyId);
        $this->validateAccessRights($survey->getUserId(), $survey->getProjectId());
        $this->votesSurveyTable->deleteBy("survey_id", $surveyId);
        $this->proposalTable->deleteBy("survey_id", $surveyId);
        $this->table->delete($surveyId);

        return $this->redirect("surveys", [
                    "id" => $projectId
        ]);
    }

    protected function validateAccessRights(int $itemUserId, int $gameId) {
        $role = $this->session->get('auth.role');
        $userId = $this->session->get('auth.user');
        if ($role == null || ($role == 'user' && $userId != $itemUserId && !$this->contributorTable->isContributor($gameId, $userId))) {
            throw new Auth\ForbiddenException();
        }
    }

}
