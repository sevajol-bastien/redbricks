<?php

namespace App\Survey\Actions;

use Framework\Renderer\RendererInterface;
use Framework\Router;
use App\Blog\Table\GameTable;
use App\News\Table\NewsTable;
use App\Blog\Table\FeatureTable;
use App\Comments\Table\CommentsTable;
use App\Bug\Table\BugTable;
use App\Survey\Table\SurveyTable;
use App\Survey\Table\VotesSurveyTable;
use Psr\Http\Message\ServerRequestInterface;
use Framework\Auth;

class SurveyListAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     *
     * @var Auth
     */
    protected $auth;

    /**
     *
     * @var VotesSurveyTable
     */
    protected $votesSurveyTable;

    public function __construct(
            RendererInterface $renderer, Router $router, GameTable $gameTable, NewsTable $newsTable, FeatureTable $featureTable, CommentsTable $commentsTable, BugTable $bugTable, SurveyTable $surveyTable, VotesSurveyTable $votesSurveyTable, Auth $auth
    ) {
        $this->renderer = $renderer;
        $this->router = $router;
        $this->gameTable = $gameTable;
        $this->newsTable = $newsTable;
        $this->bugTable = $bugTable;
        $this->commentsTable = $commentsTable;
        $this->featureTable = $featureTable;
        $this->surveyTable = $surveyTable;
        $this->votesSurveyTable = $votesSurveyTable;
        $this->auth = $auth;
    }

    /**
     * @param ServerRequestInterface $request
     * @return RedirectResponse|string
     */
    public function __invoke(ServerRequestInterface $request) {
        $slug = $request->getAttribute("slug");
        $gameId = $request->getAttribute("id");
        $game = $this->gameTable->findShow($gameId);

        if ($request->getMethod() == "POST") {
            $this->vote($request->getParsedBody());
        }

        if ($this->auth->getUser() != null) {
            $userId = $this->auth->getUser()->getId();
        } else {
            $userId = 0;
        }
        $surveys = $this->surveyTable->findCompleted($gameId, $userId);

        $nbFeatures = $this->featureTable->findShow($gameId)->where("s.id>1")->count("*");
        $nbBugs = $this->bugTable->findShow($gameId)->count("*");
        $nbComments = $this->commentsTable->findCount($gameId);
        $nbNews = $this->newsTable->findShow($gameId)->count("*");
        $nbSurveys = $this->surveyTable->findShow($gameId)->count("*");

        if ($game->getSlug() !== $slug) {
            return $this->redirect("blog.show", [
                        "slug" => $game->getSlug(),
                        "id" => $game->getId()
            ]);
        }

        return $this->renderer->render('@survey/list', [
                    "game" => $game,
                    "surveys" => $surveys,
                    "nbFeatures" => $nbFeatures,
                    "nbBugs" => $nbBugs,
                    "nbComments" => $nbComments,
                    "nbNews" => $nbNews,
                    "nbSurveys" => $nbSurveys
        ]);
    }

    private function vote(array $params) {
        $user = $this->auth->getUser();
        $surveyId = $params['surveyId'];
        $proposalId = $params['proposal'];
        $this->votesSurveyTable->insert([
            "user_id" => $user->getId(),
            "survey_id" => $surveyId,
            "proposal_id" => $proposalId
        ]);
    }

}
