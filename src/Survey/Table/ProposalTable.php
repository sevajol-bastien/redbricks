<?php

namespace App\Survey\Table;

use Framework\Database\Table;
use App\Survey\Entity\Proposal;

class ProposalTable extends Table {

    protected $entity = Proposal::class;
    protected $table = "proposals";

}

