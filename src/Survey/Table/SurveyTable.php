<?php

namespace App\Survey\Table;

use Framework\Database\Table;
use App\Survey\Entity\Survey;
use App\Survey\Table\ProposalTable;
use App\Survey\Table\VotesSurveyTable;
use Framework\Database\Query;

class SurveyTable extends Table {

    protected $entity = Survey::class;
    protected $table = "survey";

    /**
     * 
     * @param int $gameId
     * @return Query
     */
    public function findShow(int $gameId): Query {
        return $this->findAll()
                        ->where("s.project_id = $gameId");
    }

    /**
     * 
     * @param int $gameId
     * @param int $userId
     * @return array|null
     */
    public function findCompleted(int $gameId, int $userId): ?array {
        $surveys = $this->findShow($gameId);
        $proposal = new ProposalTable($this->pdo);
        $vote = new VotesSurveyTable($this->pdo);

        $proposals = $proposal->findAll()
                ->where("p.project_id=$gameId")
                ->order("p.survey_id, id");
        $votes = $vote->findAll()
                ->select("proposal_id, COUNT(*) as nb")
                ->groupBy("proposal_id")
                ->order("proposal_id");
        $votesUser = $vote->findAll()
                ->select("survey_id")
                ->where("v.user_id = $userId")
                ->fetchColumn();
        foreach ($proposals as $p) {
            $proposalsArray[$p->getSurveyId()][] = $p;
        }
        foreach ($votes as $v) {
            $votesArray[$v->getProposalId()] = $v;
        }
        $result = null;
        foreach ($surveys as $survey) {
            $result[$survey->getId()] = $survey;
            $result[$survey->getId()]->proposals = $proposalsArray[$survey->getId()];
            $nbVotes = 0;
            foreach ($result[$survey->getId()]->proposals as $key => $p) {
                if (isset($votesArray[$p->getId()])) {
                    $result[$survey->getId()]->proposals[$key]->nb = $votesArray[$p->getId()]->nb;
                    $nbVotes += $votesArray[$p->getId()]->nb;
                } else {
                    $result[$survey->getId()]->proposals[$key]->nb = 0;
                }
            }
            $result[$survey->getId()]->nb = $nbVotes;
            if (in_array($survey->getId(), $votesUser)) {
                $result[$survey->getId()]->vote = true;
            }
        }
        return $result;
    }

}
