<?php

namespace App\Survey\Table;

use Framework\Database\Table;
use App\Survey\Entity\VotesSurvey;

class VotesSurveyTable extends Table {

    protected $entity = VotesSurvey::class;
    protected $table = "votes_survey";

}

