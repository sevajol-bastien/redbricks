<?php
namespace App\User;

use Framework\Renderer\RendererInterface;

class UserDashboardAction
{

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var UserWidgetInterface[]
     */
    private $widgets;
    
    public function __construct(RendererInterface $renderer, array $widgets)
    {
            $this->renderer= $renderer;
            $this->widgets= $widgets;
    }
    public function __invoke()
    {
        
        $widgets= array_reduce($this->widgets, function (string $html, UserWidgetInterface $widgets) {
            return $html . $widgets->render();
        }, "");
        return $this->renderer->render("@user/dashboard", compact("widgets"));
    }
}
