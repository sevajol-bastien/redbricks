<?php
namespace App\User;

use Framework\Auth;

class UserTwigExtension extends \Twig_Extension
{

    /**
     * @var array
     */
    private $widgets;
    

    /**
     * @var Auth
     */
    private $auth;

    
    public function __construct(array $widgets, Auth $auth)
    {
        $this->widgets= $widgets;
        $this->auth = $auth;
    }
    
    public function getFunctions(): array
    {
        return [
            new \Twig_SimpleFunction("user_menu", [$this, "renderMenu"], ["is_safe" => ["html"]]),
            new \Twig_SimpleFunction('current_user', [$this->auth, 'getUser'])
        ];
    }
    
    public function renderMenu(): string
    {
        return array_reduce($this->widgets, function (string $html, UserWidgetInterface $widgets) {
            return $html . $widgets->renderMenu();
        }, "");
    }
}
