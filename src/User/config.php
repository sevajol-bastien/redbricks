<?php
use App\User\UserTwigExtension;
use App\User\UserModule;
use App\User\UserDashboardAction;

return [
    "user.prefix" => "/user",
    "user.widgets" => [],
    UserTwigExtension::class => \DI\object()->constructor(\DI\get("user.widgets")),
    UserModule::class => \DI\object()->constructorParameter("prefix", \DI\get("user.prefix")),
    UserDashboardAction::class => \DI\object()->constructorParameter("widgets", \DI\get("user.widgets"))
];
