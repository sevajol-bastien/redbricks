<?php

namespace App\Webhook\Actions;

use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Shop\Table\SubscriptionTable;
use App\Shop\Table\RewardTable;
use Framework\Api\Stripe;

class PaiementAction {

    /**
     *
     * @var Renderer
     */
    protected $renderer;

    /**
     *
     * @var SubscriptionTable
     */
    protected $subscriptionTable;
    
    /**
     *
     * @var RewardTable
     */
    protected $rewardTable;
    
    /**
     *
     * @var String
     */
    protected $stripe;
    protected $event = null;

    public function __construct(
            RendererInterface $renderer, SubscriptionTable $subscriptionTable, RewardTable $rewardTable, Stripe $stripe
    ) {
        $this->renderer = $renderer;
        $this->subscriptionTable = $subscriptionTable;
        $this->rewardTable = $rewardTable;
        $this->stripe = $stripe;
    }

    public function __invoke(Request $request) {
        $payload = file_get_contents('php://input');

        try {
            $event = \Stripe\Event::constructFrom(
                            json_decode($payload, true)
            );
        } catch (\UnexpectedValueException $e) {
            // Invalid payload
            http_response_code(400);
            exit();
        }

        // Handle the event
        switch ($event->type) {
            case 'checkout.session.completed':
                $checkoutSession = $event->data->object; // contains a \Stripe\Checkout\
                if ($checkoutSession['subscription'] != null) {
                    $stripeAccount = $checkoutSession['client_reference_id'];
                    $subscription = $this->stripe->getSubscription($checkoutSession['subscription'], ["stripe_account" => $stripeAccount]);
                    $metadata = $subscription['metadata'];
                    $rewardId = $metadata['reward_id'];
                    $this->subscriptionTable->insert([
                        "user_id" => $metadata['user_id'],
                        "game_id" => $metadata['game_id'],
                        "reward_id" => $rewardId,
                        "amount" => $subscription['plan']['amount'],
                        "subscription_id" => $subscription['id'],
                        "plan_id" => $subscription['plan']['id'],
                        "stripe_account" => $stripeAccount,
                        "created_at" => date('Y-m-d H:i:s')
                    ]);
                    if ($rewardId != 0) {
                        $reward = $this->rewardTable->find($rewardId);
                        $this->rewardTable->update($rewardId, [
                           "contributors_nb" =>  $reward->getContributorsNb() + 1 
                        ]);
                    }
                        
                    
                }
                break;
            default:
                // Unexpected event type
                http_response_code(400);
                exit();
        }

        http_response_code(200);
        exit();
    }

}
