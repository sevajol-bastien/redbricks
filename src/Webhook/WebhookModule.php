<?php

namespace App\Webhook;

use Framework\Module;
use Framework\Router;
use Framework\Renderer\RendererInterface;
use App\Webhook\Actions\PaiementAction;

class WebhookModule extends Module {

    public function __construct(Router $router, RendererInterface $renderer) {
        $router->get('/webhook/paiement', PaiementAction::class, 'webhook.paiement');
        $router->post('/webhook/paiement', PaiementAction::class);
    }

}
